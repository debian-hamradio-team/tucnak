/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __LANGUAGE2_H
#define __LANGUAGE2_H

extern char dummyarray[];

extern int current_language;

void init_trans(void);
void shutdown_trans(void);
char *get_text_translation(char *);
char *get_english_translation(char *);
void set_language(int);
int n_languages(void);
char *language_name(int);

#define _(_x_) get_text_translation(_x_)
#define CTEXT(x) (dummyarray + x)
#define VTEXT(_x_) get_text_translation(CTEXT(_x_))




#endif
