/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2006  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/
#include "header.h"

#include "cwdaemon.h"
#include "fifo.h"
#include "rc.h"
#include "ttys.h"

int ttys_init(struct cwdaemon *cwda){
#ifdef HAVE_TERMIOS_H
	cwda->zser = zserial_init_tty(cfg->cwda_device);
#elif defined(WIN32)
	cwda->zser = zserial_init_win32(cfg->cwda_device);
#endif
	zserial_set_line(cwda->zser, 9600, 8, 'N', 1);
	zserial_nolocks(cwda->zser, cfg->nolocks);
    ttys_open(cwda, 1);
	return 0;
}

int ttys_open(struct cwdaemon *cwda, int verbose){
    if (zserial_open(cwda->zser)){
		if (verbose) log_addf("%s", zserial_errorstr(cwda->zser));
        ttys_reset(cwda);
//        ttys_free(cwda);
        return -1;
    }
    ttys_reset(cwda);
    return 0;
}

int ttys_free(struct cwdaemon *cwda){
    if (!cwda) return 0;

	zserial_free(cwda->zser);
	return 0;
}

int ttys_reset(struct cwdaemon *cwda){
    cwda->ptt(cwda, 0);
    cwda->cw(cwda, 0);
    cwda->ssbway(cwda, 0);
	return 0;
}

int ttys_cw(struct cwdaemon *cwda, int onoff){
	if (zserial_dtr(cwda->zser, onoff)){
		error("%s\n", zserial_errorstr(cwda->zser));
        return -1;
	}
    return 0;
}

int ttys_ptt(struct cwdaemon *cwda, int onoff){
	if (zserial_rts(cwda->zser, onoff)){
		error("%s\n", zserial_errorstr(cwda->zser));
        return -1;
	}
    return 0;
}


// for U5 link and FT8x7
int ttys_cw_single(struct cwdaemon *cwda, int onoff){
	if (zserial_dtr(cwda->zser, onoff)){
		error("%s\n", zserial_errorstr(cwda->zser));
		return -1;
	}
	return 0;
}

// for U5 link and FT8x7
int ttys_ptt_single(struct cwdaemon *cwda, int onoff){
	if (gses != NULL && gses->mode == MOD_CW_CW)   // skipped during init
	{
		onoff = 0; // NO PTT in CW
	}
	else{
		// in SSB do PTT
	}

	if (zserial_rts(cwda->zser, onoff)){
		error("%s\n", zserial_errorstr(cwda->zser));
		return -1;
	}
	return 0;
}



int ttys_ssbway(struct cwdaemon *cwda, int onoff){
	return 0;
}

