/*
    Tucnak - SOTA spot sender
    Copyright (C) 2002-2020  Ladislav Vaiz <ok1zia@nagano.cz>;
    2020 Michal OK2MUF

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SOTA_SPOT_H
#define __SOTA_SPOT_H


#define SOTA_API_INSECURE 1

/*
{
  "id": 0,
  "userID": 0,
  "timeStamp": "2020-11-03T21:01:44.160Z",
  "comments": "string",
  "callsign": "string",
  "associationCode": "string",
  "summitCode": "string",
  "activatorCallsign": "string",
  "activatorName": "string",
  "frequency": "string",
  "mode": "string",
  "summitDetails": "string",
  "highlightColor": "string"
}
*/

struct sota_spot_data {
    int id;
    int userID;
    char *timeStamp;
    char comment[128];
    char *callsign;
    char *associationCode;
    char *summitCode;
    char *activatorCallsign;
    char *activatorName;
    char frequency[16];
    char *mode;
    char *summitDetails;
    char *highlightColor;
    char _summit[16];
};

void sota_spot_push(void);
void sota_spot_send(struct sota_spot_data **ssd);

#endif
