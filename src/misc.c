/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2020 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include <libzia.h>

#include "header.h"
#include "main.h"
#include "misc.h"
#include "terminal.h"


#include <stdio.h>
void test(void){
   /* char s[100];
    char n[2014];
    FILE *f;

    strcpy(n, "e:\\home/tucnak/20110507//log");
    f = fopen(n, "rt");
    if (f != NULL){
	char *dummy = fgets(s, 80, f);
	fclose(f);
    }*/


}

void bat_info(GString *gs){
    struct zbat *zbat;

    g_string_append_printf(gs, "\n  bat_info:\n");

    zbat = zbat_init();
    zbat_getinfo(zbat);
    if (zbat->n == 0){
        g_string_append_printf(gs, "No battery detected\n");
    }else{
        g_string_append_printf(gs, "Batteries: %d\n", zbat->n);
        g_string_append_printf(gs, "Capacity: %d%%\n", zbat->capacity);
        if (zbat->technology != NULL) {
            g_string_append_printf(gs, "Technology: %s\n", zbat->technology);
        }
    }
	g_string_append_printf(gs, "\n");
    zbat_free(zbat);
}


#ifdef Z_HAVE_SDL

struct zbat *gbat;

void bat_timer(void *a){
	zbat_getinfo(gbat);
	zselect_timer_new(zsel, 20000, bat_timer, NULL);
	redraw_later();
}
#endif

void serial_info(GString *gs){
    struct zserial *zser;

    g_string_append_printf(gs, "\n  serial_info:\n");

    zser = zserial_init_serial(NULL);
    if (!zser) return;
    zserial_detect(zser);
    if (zser->ports->len == 0){
        g_string_append_printf(gs, "No serial ports detected\n");
    }else{
        int i;
        for (i = 0; i < zser->ports->len; i++){
            struct zserial_port *port = (struct zserial_port *)g_ptr_array_index(zser->ports, i);
            g_string_append_printf(gs, "%s %s\n", port->filename, port->desc);
        }
    }
	g_string_append_printf(gs, "\n");
    zserial_free(zser);
}

/*const char *iconv_current_encoding(){
#ifdef Z_MSC_MINGW
	static char cp[20];
	g_snprintf(cp, sizeof(cp), "CP%d", GetACP());
	return cp;
#endif
} */

