/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __MASTERDB_H
#define __MASTERDB_H

#include "header.h"

struct masterdb{
    ZHashTable *masters; /* key=call, value=name */
};

extern struct masterdb *masterdb;  
  
struct masterdb *init_masterdb(void);
void free_masterdb(struct masterdb *masterdb);
void add_masterdb(struct masterdb *masterdb, gchar *call);
gint get_masterdb_size(struct masterdb *masterdb);
void load_one_masterdb(struct masterdb *masterdb, gchar *s);
int load_masterdb_from_mem(struct masterdb *masterdb, const char *file, const long int len);
int load_masterdb_from_file(struct masterdb *masterdb, gchar *filename);
void read_masterdb_files(struct masterdb *masterdb);
gchar *masterdb_search(struct masterdb *masterdb, char *pattern, int maxreslen);

#endif
