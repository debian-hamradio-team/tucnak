/*
    Tucnak - VHF contest log
    Copyright (C) 2012-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ANDROID_H
#define __ANDROID_H

#define PACKAGE "tucnak"
#define VERSION Z_VERSION
#define PACKAGE_NAME PACKAGE
#define PACKAGE_VERSION VERSION
#define PKG "apk"

#define HAVE_UINT32_T
#define HAVE_SDL
#define HAVE_ICONV_H
#define HAVE_PNG_H
#define HAVE_LIBPNG
#undef HAVE_SNDFILE
#undef HAVE_PORTAUDIO
#undef HAVE_SLES_OPENSLES_H
#undef HAVE_LIBFFTW3
#undef USE_FFT
//#define HAVE_HAMLIB
//#define HAVE_FTDI_NEW
#define HAVE_FTDI_USB_GET_STRINGS
#define HAVE_SYS_SELECT_H
//#define HAVE_MATH_H
#define HAVE_UNISTD_H
#define HAVE_TERMIOS_H
#define HAVE_SYS_WAIT_H
#define DDIR ""
#define HAVE_NETDB_H 
#define HAVE_SYS_SOCKET_H

#include "regex_.h"

int android_main(int argc, char *argv[]);
void android_restore_state(void);
void android_update_package(char *filename);

#endif
