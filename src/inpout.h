/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __INPOUT_H
#define __INPOUT_H

struct cwdaemon;

#if defined(Z_MSC_MINGW_CYGWIN)
int parport_init   (struct cwdaemon *);
int parport_open   (struct cwdaemon *cwda, int verbose);
int parport_free   (struct cwdaemon *);
int parport_reset  (struct cwdaemon *);
int parport_cw     (struct cwdaemon *, int onoff);
int parport_ptt    (struct cwdaemon *, int onoff);
int parport_ssbway (struct cwdaemon *, int onoff);
int parport_band   (struct cwdaemon *, int bandsw);
#endif

#endif
