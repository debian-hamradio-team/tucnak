/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __MISC_H
#define __MISC_H

void test(void);
void bat_info(GString *gs);

#ifdef Z_HAVE_SDL
extern struct zbat *gbat;
void bat_draw(SDL_Surface *surface, int x, int y, int w, int h, int capacity);
void bat_timer(void *);
#endif

void serial_info(GString *gs);

//const char *iconv_current_encoding(void);


#endif
