/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2020 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __FIFO_H
#define __FIFO_H

struct fifo{
    int x,y,w,h,maxlen,ho;
    int withouttime;
    GPtrArray *items;
};

#define log_adds(s) fifo_adds(glog, s) 

extern struct fifo *glog, *gtalk;
struct fifo *init_fifo(int maxlen);
void fifo_resize(struct fifo *log, int x, int y, int w, int h);
void free_fifo(struct fifo *fifo);
void drop_fifo(struct fifo *fifo);
void fifo_adds(struct fifo *fifo, gchar *str);
void fifo_addf(struct fifo *fifo, char *m, ...); 
void fifo_addfq(struct fifo *fifo, char *m, ...); 
void log_addf(char *m, ...); 
int fifo_len(struct fifo *fifo);
gchar *fifo_index(struct fifo *fifo, int);
int save_fifo_to_file(struct fifo *fifo, gchar *filename);
int load_fifo_from_file(struct fifo *fifo, gchar *filename, int drop);
void log_draw(struct fifo *fifo);
void fifo_add_lines(struct fifo *fifo, char *data);
int fifo_contains(struct fifo *fifo, char *needle);
char *fifo_call_under(struct fifo *fifo, int x, int y);
void fifo_message(void *itdata, void *menudata);
void fifo_kst_info(void *itdata, void *menudata);
#ifdef Z_HAVE_SDL
void fifo_kst_ac_info(void *itdata, void *menudata);
#endif

#endif
