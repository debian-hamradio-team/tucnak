/*
    mingw.h - mingw32 specific functions
    Copyright (C) 2011-2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/


#ifndef __TUCNAK_MINGW_H
#define __TUCNAK_MINGW_H

#ifndef _WIN32_IE
#define _WIN32_IE 0x0400
#endif

#include <winsock2.h>
#include <windows.h>
    
#include "regex_.h"

//#define PKG "nsis"

int init_mingw(void);

#endif
