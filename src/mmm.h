/*
    Tucnak - VHF contest log
    Copyright (C) 2022 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __MMM_H
#define __MMM_H

#include "header.h"

struct namedb;
struct cw;

int load_mmm_from_file(struct cw *cw, struct namedb *namedb, char *filename, int *nlocs, int *nnames);

#endif
