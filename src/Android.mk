LOCAL_PATH:= $(call my-dir)

include ../../libzia/src/Android.mk
#include ../../glib/Android.mk
#include ../../libiconv/jni/Android.mk
#include ../../libsdl/project/jni/Android.mk
#include ../../hamlib/Android.mk
#include ../../libftdi/jni/Android.mk
include ../../libsndfile/jni/Android.mk


include $(CLEAR_VARS)
LOCAL_PATH := /home/ja/c/tucnak/src

LOCAL_SRC_FILES := \
    ac.c acadsbone.c acairdk.c acdump1090.c acfile.c acosn.c \
	adif.c android.c bfu.c cabrillo.c \
	charsets.c \
    chart.c \
	control.c cordata.c \
	cwdaemon.c cwdb.c cwwindow.c \
	davac4.c dsp.c dwdb.c dxc.c \
	edi.c \
	error.c excdb.c fft.c \
	fifo.c \
	hf.c html.c \
	httpd.c \
	icons.c inpout.c inputln.c \
	kbd.c \
	kbdbind.c \
	kst.c \
	language.c \
	list.c main.c map.c masterdb.c \
	menu.c menu1.c menu2.c menu3.c menu4.c menu5.c menu6.c menu7.c \
	mingw.c misc.c mmm.c namedb.c net.c \
	oct8tor.c os_dep.c pa.c player.c ppdev.c \
	profile.c qrvdb.c qsodb.c \
    rain.c \
    rc.c report.c rotar.c \
    scope.c sdev.c \
	session.c sdrc.c sked.c slovhfnet.c \
    sndf.c \
	sota.c sota_api.c sota_spot.c sota_upload.c \
	ssbd.c state.c stats.c stf.c subwin.c terminal.c \
	titlpage.c translate.c tregex.c trig.c \
	tsdl.c ttys.c txts.c \
	update.c \
	winkey.c wiki.c wizz.c \
    zosk.c \
	zstring.c 

LOCAL_MODULE := application

LOCAL_CFLAGS := -fPIC -std=gnu99 -g
LOCAL_LDFLAGS := -fPIC
LOCAL_STRIP_MODE := none
LOCAL_STRIP_MODULE := keep_symbols

LOCAL_C_INCLUDES := \
	$(GLIB_TOP) \
	$(GLIB_TOP)/glib \
	$(GLIB_TOP)/android	\
	$(GLIB_TOP)/android-internal \
    $(SDL_TOP)/jni/sdl-1.2/include \
    $(SDL_TTF_TOP) \
    $(PNG_TOP)/jni \
    $(USB_TOP)/jni \
    $(FTDI_TOP)/src \
    $(ZIA_TOP)/include \
    $(ZIA_TOP)/include/android \
    $(ICONV_TOP)/include \
    $(OPENSSL_TOP)/include \
    $(HAMLIB_TOP)/include \
    $(PA_TOP)/include \
    $(SNDFILE_TOP)/jni 



LOCAL_LDLIBS := \
    -L$(ZIA_TOP)/src/obj/local/armeabi \
    -L$(ICONV_TOP)/libs/armeabi-v7a \
    -L$(OPENSSL_TOP) \
    -L$(GLIB_TOP)/obj/local/armeabi \
    -L$(SDL_TOP)/project/libs/armeabi \
    -L$(HAMLIB_TOP)/libs/armeabi \
    -L$(USB_TOP)/libs/armeabi -L$(FTDI_TOP)/libs/armeabi \
    -llog \
    -L$(SNDFILE_TOP)/libs/armeabi \

LOCAL_STATIC_LIBRARIES := zia iconv glib-2.0 gthread-2.0 sdl-1.2 sndfile #hamlib


include $(BUILD_SHARED_LIBRARY)
#include $(LOCAL_SHARED_LIBRARY)

