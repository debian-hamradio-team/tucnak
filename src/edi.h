/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __EDI_H
#define __EDI_H

#include "header.h"

struct qso;
struct band;
struct contest;

#define MAX_EXC_LEN 30

int save_all_bands_txt(int is_autosave);
int check_autosave(void);
void load_contest_edi(const gchar *date, int edi);
void add_qso_str1(GString *gs, struct qso *q, struct band *b);
void add_qso_str2(GString *gs, struct qso *q, struct band *b);
void add_qso_str3(GString *gs, struct qso *q, struct band *b);
const char *get_psect_str(struct band *b); // returns foreign key
void import_edi (void *xxx, char *filename);
void import_swap(void *xxx, char *filename);
void dump_all_sources(struct contest *ctest);
int save_desc_to_file(gchar *filename);
int export_all_bands_edi(void);

#endif
