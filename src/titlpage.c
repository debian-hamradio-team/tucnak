/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2015  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"
#include "bfu.h"
#include "fifo.h"
#include "html.h"
#include "qsodb.h"
#include "rc.h"
#include "stats.h"
#include "tsdl.h"

#define RESP cfg

int export_all_bands_titlpage(){
    struct band *band;
    int i,ptr, idlen, filetemplate;
    gchar *filename;
    FILE *f;
    char callbuf[20];
    int ignoreerror=0;
    gchar *template_;
    gchar s[4100];
    char *c;
    GString *gs;
    
    
    if (!ctest) return -1;
    
    dbg("export_all_bands_titlpage()\n");

	
    filetemplate=0;
#if 0
    f=fopen(SHAREDIR"/titlpage.html", "rt");
    if (!f){
       /* log_addf(VTEXT(T_CANT_OPEN_S), SHAREDIR"/titlpage.html");*/
        goto mem;
    }
   
    
    if (fstat(fileno(f), &st)){
        fclose(f);
/*        log_addf(VTEXT(T_CANT_READ_S), SHAREDIR"/titlpage.html");   */
        goto mem;
    }
   
    template_=g_new0(gchar, st.st_size+1);
    if (fread(template_, st.st_size, 1, f)!=1){
        fclose(f);
  /*      log_addf(VTEXT(T_CANT_READ_S), SHAREDIR"/titlpage.html");*/
        goto mem;
    }
    fclose(f);
    filetemplate=1;
mem:;
#endif

    if (!filetemplate) template_=g_strdup(txt_titlpage);
            
    
    gs=g_string_new("");
    
    for (i=0; i<ctest->bands->len; i++){
        band = (struct band*)g_ptr_array_index(ctest->bands, i);

	    stats_thread_join(band);
        if (band->stats->nqsos <=0) continue;

		progress(VTEXT(T_EXPORTING_S), band->bandname);
        filename = g_strdup_printf("%s/%s_%s_%c_titlepage.html",
                        ctest->directory,
                        ctest->cdate,
                        z_get_raw_call(callbuf,ctest->pcall),
                        z_char_lc(band->bandchar));

        z_wokna(filename);    
        f=fopen(filename,"wt");
        if (!f) {
            if (!ignoreerror) { errbox(VTEXT(T_CANT_WRITE), errno); ignoreerror=1;}
            g_free(filename);
			progress(NULL);
            return -1;
        }
        
        ptr=0;
        while((c=strchr(template_+ptr, '$'))!=NULL){
            fwrite(template_+ptr, c-template_-ptr, 1, f);
            idlen=strspn(c+1, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_");
            
            safe_strncpy0(s, c+1, idlen+1);
            log_addf("id=%s idlen=%d\n", s, idlen);
            ptr=c-template_+1+idlen;

            if (strcasecmp(s, "PCall")==0){
                fprintf(f, "%s", qh(gs, ctest->pcall));
            }else if(strcasecmp(s, "CAvg")==0){
                fprintf(f, "%d", band->stats->nqsos?band->stats->nqsop/band->stats->nqsos:0);
            }else if(strcasecmp(s, "CDXCCS")==0){
                fprintf(f, "%d", g_hash_table_size(band->stats->dxcs));
            }else if(strcasecmp(s, "CODXCall")==0){
                fprintf(f, "%s", qh(gs, band->stats->odxcall));
            }else if(strcasecmp(s, "CODXloc")==0){
                fprintf(f, "%s", qh(gs, band->stats->odxwwl));
            }else if(strcasecmp(s, "CODXQRB")==0){
                fprintf(f, "%d", band->stats->odxqrb_int);
            }else if(strcasecmp(s, "CQSOs")==0){
                fprintf(f, "%d", band->stats->nqsos);
            }else if(strcasecmp(s, "CToSc")==0){
                fprintf(f, "%d", band->stats->ntotal);
            }else if(strcasecmp(s, "Mope1")==0){
                fprintf(f, "%s", qh(gs, band->mope1));
            }else if(strcasecmp(s, "Mope2")==0){
                fprintf(f, "%s", qh(gs, band->mope2));
            }else if(strcasecmp(s, "NoOfPages")==0){
                fprintf(f, "%d", (band->qsos->len+79)/40);                     
            }else if(strcasecmp(s, "PAddr1")==0){
                fprintf(f, "%s", qh(gs, ctest->padr1));
            }else if(strcasecmp(s, "PAddr2")==0){
                fprintf(f, "%s", qh(gs, ctest->padr2));
            }else if(strcasecmp(s, "PBand")==0){
                fprintf(f, "%s", qh(gs, band->pband));
            }else if(strcasecmp(s, "PSect")==0){
                fprintf(f, "%d", band->psect);
            }else if(strcasecmp(s, "PSectName")==0){
                fprintf(f, "%s", qh(gs, band->psect?"Single":"Multi"));
            }else if(strcasecmp(s, "PWWLo")==0){
                fprintf(f, "%s", qh(gs, ctest->pwwlo));
            }else if(strcasecmp(s, "RAddr1")==0){
                fprintf(f, "%s", qh(gs, RESP->radr1));
            }else if(strcasecmp(s, "RAddr2")==0){
                fprintf(f, "%s", qh(gs, RESP->radr2));
            }else if(strcasecmp(s, "RAnte")==0){
                fprintf(f, "%s", qh(gs, band->sante)); /* anteny jsou stejny */
            }else if(strcasecmp(s, "RCall")==0){
                fprintf(f, "%s", qh(gs, RESP->rcall));
            }else if(strcasecmp(s, "RCity")==0){
                fprintf(f, "%s", qh(gs, RESP->rcity));
            }else if(strcasecmp(s, "RName")==0){
                fprintf(f, "%s", qh(gs, RESP->rname));
            }else if(strcasecmp(s, "RPoCo")==0){
                fprintf(f, "%s", qh(gs, RESP->rpoco));
            }else if(strcasecmp(s, "RXEq")==0){
                fprintf(f, "%s", qh(gs, band->srxeq));
            }else if(strcasecmp(s, "SAnth")==0){
                fprintf(f, "%s", qh(gs, band->santh));
            }else if(strcasecmp(s, "SASL")==0){
                /*fprintf(f, "%s", qh(gs, band->sasl));*/
            }else if(strcasecmp(s, "SPowe")==0){
                fprintf(f, "%s", qh(gs, band->spowe));
            }else if(strcasecmp(s, "SPreamp")==0){
/*                fprintf(f, "%s", qh(gs, "));*/
            }else if(strcasecmp(s, "TDate")==0){
                char t[100];
                qh(gs, ctest->cdate);
                safe_strncpy0(t, gs->str, 98);
                fprintf(f, "%c%c.%c%c.%c%c%c%c",t[6],t[7], t[4],t[5], t[0],t[1],t[2],t[3]);
            }else if(strcasecmp(s, "TName")==0){
                fprintf(f, "%s", qh(gs, ctest->tname));
            }else if(strcasecmp(s, "TXEq")==0){
                fprintf(f, "%s", qh(gs, band->stxeq));
            }else{
                fprintf(f, "XXXXXXXXXXX");
            }
            
        }
        fprintf(f, "%s", template_+ptr);
        
        
	    fclose(f);
	    log_addf(VTEXT(T_SAVED_S), filename);
    	g_free(filename);
        


    }
    g_string_free(gs, TRUE);
	
/*	if (err) {
		if (!ignoreerror) { errbox(VTEXT(T_CANT_WRITE), 0); ignoreerror=1; }
		g_free(filename);
		return -1;
	}    */
	progress(NULL);
    return 0;
}

