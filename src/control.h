/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2023 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __CONTROL_H
#define __CONTROL_H

#include "qsodb.h"

enum wt_type{
    WT_CLEAR=0,
    WT_CALLSIGN,
    WT_RSTS,
    WT_RSTR,
    WT_QSONRS,
    WT_QSONRR,
    WT_EXC,
    WT_LOCATOR,
    WT_OPERATOR,
	WT_REMARK,       /* unused */
	WT_BAND
};

struct spypeer{
    gchar *callsign;
    gchar *rsts,*rstr;
    gchar *qsonrs,*qsonrr;
    gchar *exc, *locator;
    gchar *remark, *operator_;
    
    gint peertx;
    gchar *inputline;

    gchar *peerid;
    gchar bandchar, abandchar;
};


void menu_forcerun(void *arg);
/*void menu_spy(void *arg);*/
void menu_endspy(void *arg);
int can_tx(struct band *b);
int can_cq(struct band *b);
void menu_grabband(void *arg);
void net_grab(struct band *b, enum ccmd ccmd, char *netid);
void ctrl_back(struct band *b);
void wkd_tmpqso(struct band *band, enum wt_type type, gchar *call);
void wkd_send_all(struct band *b);
void peer_tx(struct band *band, int tx);
void do_endspy(void *arg, void *arg1);
void menu_endspy(void *arg);
void send_inputline(struct band *band);

struct spypeer *init_spypeer(GPtrArray *sps, gchar *peerid, char bandchar);
void spypeer_send_request(struct spypeer *sp, char bandchar);
void spypeer_send_requests(void);
struct spypeer *get_spypeer_by_peerid(GPtrArray *sps, gchar *peerid, char bandchar);
void free_spypeer(struct spypeer *sp);
void free_spypeers(GPtrArray *sps);
void clear_spypeer(struct spypeer *sp);
void send_spypeer_request(void *arg);
void spypeer_add(char *peerid, char bandchar, int send_request);
int load_spypeers_from_file(char *filename);
int save_spypeers_to_file(char *filename);


#endif
