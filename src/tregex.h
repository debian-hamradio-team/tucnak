/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __TREGEX_H
#define __TREGEX_H

#include "header.h"

#define MAX_MATCHES 10

int regcmp(char *string, char *regex);
int regcmpi(char *string, char *regex);

#ifdef LEAK_DEBUG_LIST
#define regmatch(string, regex...) debug_regmatch(__FILE__, __LINE__, string, regex)
int debug_regmatch(char *file, int line, const char *string, char *regex, ...);
#else
int regmatch(const char *string, char *regex, ...);
#endif

#endif
