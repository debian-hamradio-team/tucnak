/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SCOPE_H
#define __SCOPE_H

#include "header.h"
struct subwin;
struct event;

int  sw_scope_kbd_func(struct subwin *sw, struct event *ev, int fw);
int  sw_scope_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_scope_redraw(struct subwin *sw, struct band *band, int flags);
void sw_scope_check_bounds(struct subwin *sw);
void sw_scope_raise(struct subwin *sw);
void sw_scope_clear_scopes(void);


#endif
