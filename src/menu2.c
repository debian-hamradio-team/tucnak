/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2021  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"

#include "bfu.h"
#include "chart.h"
#include "edi.h"
#include "map.h"
#include "menu.h"
#include "net.h"
#include "qrvdb.h"
#include "qsodb.h"
#include "rain.h"
#include "session.h"
#include "sked.h"
#include "stats.h"
#include "terminal.h"


/******************** MODES *********************************/

#define MAX_MODES 9

gint mode;
char mode_str[EQSO_LEN];

void mode_func (void *arg){
    int active;

    active=GPOINTER_TO_INT(arg);
    if (active<0 || active>MAX_MODES) return;
    mode = active;
    safe_strncpy0(mode_str, get_text_translation(mode_msg[mode]),EQSO_LEN);
    /*safe_strncpy0(mode_str,_(CTEXT(T_QP0+qsop_method),term),MAX_STR_LEN);*/
    redraw_later();
}

int dlg_mode(struct dialog_data *dlg, struct dialog_item_data *di){
    int i, sel;
    struct menu_item *mi;
    
    if (!(mi = new_menu(1))) return 0;
    for (i = 1; i <= MAX_MODES; i++) {
        add_to_menu(&mi, mode_msg[i], "", "", MENU_FUNC mode_func, GINT_TO_POINTER(i), 0);
    }
    sel = mode-1;
    if (sel < 0) sel = 0;
    if (sel>=MAX_MODES) sel=0;
    do_menu_selected(mi, GINT_TO_POINTER(mode), sel);
    return 0;
}

/******************** EDIT QSO *******************************/

static char callsign[EQSO_LEN], rstr[EQSO_LEN], qsonrr[EQSO_LEN], exc[MAX_EXC_LEN+1], locator[EQSO_LEN];
/*gint mode;
char mode_str[EQSO_LEN]*/
char rsts[EQSO_LEN], qsonrs[EQSO_LEN];
static char date_str[EQSO_LEN], time_str[EQSO_LEN], operator_[EQSO_LEN], remark[MAX_STR_LEN], phase_str[EQSO_LEN];
gint qerror,qsl;

#define OEQSO_LEN 24
#define QRG_LEN 50
char ocallsign[OEQSO_LEN], orstr[OEQSO_LEN], oqsonrr[OEQSO_LEN], oexc[MAX_EXC_LEN+1], olocator[OEQSO_LEN];
char omode_str[OEQSO_LEN], orsts[OEQSO_LEN], oqsonrs[OEQSO_LEN];
char odate_str[OEQSO_LEN], otime_str[OEQSO_LEN], ooperator[OEQSO_LEN], oremark[MAX_STR_LEN], oqrg[QRG_LEN], ophase_str[OEQSO_LEN]; 

char oomode_str[OEQSO_LEN], oorstr[OEQSO_LEN], oorsts[OEQSO_LEN];

int pbandi=0;
#define TIME_DELIM ":."
void refresh_edit_qso(struct qso *q)
{
    char *token_ptr, *h, *m;
	char raw[30];
	struct qso *qq;


    
/*    dbg("refresh_edit_qso\n");*/
    dump_qso(q, "refresh_edit_qso before");

	z_get_raw_call(raw, q->callsign);
	g_hash_table_remove(q->band->rawqsoshash, raw);
	

    if ((strcmp(rstr,oorstr) || strcmp(rsts,oorsts)) &&
         !strcmp(mode_str,oomode_str)){

            if (mode == MOD_SSB_SSB ||
                mode == MOD_CW_SSB ||
                mode == MOD_SSB_CW ||
                mode == MOD_CW_CW){
                
                int s,r;
                /* CHANGE menu2.c refresh_edit_qso */
                s=strlen(rsts);
                r=strlen(rstr);
                if (s==3 && r==3) mode=MOD_CW_CW;
                if (s==2 && r==2) mode=MOD_SSB_SSB;
                if (s==3 && r==2) mode=MOD_CW_SSB;
                if (s==2 && r==3) mode=MOD_SSB_CW;
            }
    }
            
    m = NULL;
    h = strtok_r(time_str, TIME_DELIM, &token_ptr);
    if (h) m = strtok_r(NULL, TIME_DELIM, &token_ptr);
    if (m){
        sprintf(time_str, "%02d%02d", atoi(h)%24, atoi(m)%60);
    }else{
        sprintf(time_str, "%02d%02d", atoi(time_str)/100, atoi(time_str)%100);
    }
    
    STORE_STR_FS   (q,time_str);

    STORE_STR_FS_UC(q,operator_);
    STORE_STR_FS   (q,date_str);
    STORE_STR_FS_UC(q,callsign);
    STORE_INT      (q,mode);
    STORE_STR_FS_UC(q,rsts);
    STORE_STR_FS_UC(q,rstr);
    STORE_STR_FS   (q,qsonrr);
    STORE_STR_FS_UC(q,exc);
    STORE_STR_FS_UC(q,locator);
    STORE_STR_FS   (q,remark);
	STORE_SINT     (q, phase);
    q->error = qerror;
    q->qsl   = qsl;


    q->stamp = time(NULL);
    g_hash_table_foreach(ctest->bystamp, foreach_source_qsort_by_stamp, NULL);
    z_ptr_array_qsort(ctest->allqsos, compare_date_time_qsonrs);
   
    compute_qrbqtf(q);
    dirty_band(aband);
    write_qso_to_swap(aband, q);
    replicate_qso(NULL, q);
   // recalc_stats(aband);
    recalc_worked_skeds(aband);
    qrv_recalc_wkd(qrv);
    qrv_recalc_qrbqtf(qrv);
    qrv_recalc_gst(qrv);
	
	if (!q->dupe && !q->error){
		z_get_raw_call(raw, q->callsign);
		qq = (struct qso *)g_hash_table_lookup(q->band->rawqsoshash, raw);
		if (!qq) g_hash_table_insert(q->band->rawqsoshash, g_strdup(raw), q);
	}

    /*check_autosave();*/
#ifdef Z_HAVE_SDL
	maps_reload();
	rain_reload();
#endif
    chart_reload();    
    dump_qso(q, "refresh_edit_qso after");
    dump_all_sources(ctest);
}                    

char *edit_qso_msg[] = {
    ocallsign, /* 0 */
    orstr    ,
    oqsonrr  ,
    oexc     ,
    olocator ,
    omode_str, /* 5 */
    orsts    ,
    /*oqsonrs  ,*/
    odate_str,
    otime_str,
    ooperator, 
    oremark  , /* 10 */
	ophase_str,
    /*oqrg, */
    CTEXT(T_ERROR),
    CTEXT(T_QSL),
    "", /* OK */  /* 14 */
    "", /* Cancel */
};


void edit_qso_fn(struct dialog_data *dlg)
{
    struct terminal *term = dlg->win->term;
    int max = 0, min = 0;
    int w, rw;
    int y = -1;

    max_group_width(term, edit_qso_msg + 0 , dlg->items + 0, 1, &max);
    min_group_width(term, edit_qso_msg + 0 , dlg->items + 0, 1, &min);
    max_group_width(term, edit_qso_msg + 1 , dlg->items + 1, 1, &max);
    min_group_width(term, edit_qso_msg + 1 , dlg->items + 1, 1, &min);
    max_group_width(term, edit_qso_msg + 2 , dlg->items + 2, 1, &max);
    min_group_width(term, edit_qso_msg + 2 , dlg->items + 2, 1, &min);
    max_group_width(term, edit_qso_msg + 3 , dlg->items + 3, 1, &max);
    min_group_width(term, edit_qso_msg + 3 , dlg->items + 3, 1, &min);
    max_group_width(term, edit_qso_msg + 4 , dlg->items + 4, 1, &max);
    min_group_width(term, edit_qso_msg + 4 , dlg->items + 4, 1, &min);
    max_group_width(term, edit_qso_msg + 5 , dlg->items + 5, 1, &max);
    min_group_width(term, edit_qso_msg + 5 , dlg->items + 5, 1, &min);
    max_group_width(term, edit_qso_msg + 6 , dlg->items + 6, 1, &max);
    min_group_width(term, edit_qso_msg + 6 , dlg->items + 6, 1, &min);
    max_group_width(term, edit_qso_msg + 7 , dlg->items + 7, 1, &max);
    min_group_width(term, edit_qso_msg + 7 , dlg->items + 7, 1, &min);
    max_group_width(term, edit_qso_msg + 8 , dlg->items + 8, 1, &max);
    min_group_width(term, edit_qso_msg + 8 , dlg->items + 8, 1, &min);
    max_group_width(term, edit_qso_msg + 9 , dlg->items + 9, 1, &max);
    min_group_width(term, edit_qso_msg + 9 , dlg->items + 9, 1, &min);
    max_group_width(term, edit_qso_msg +10 , dlg->items +10, 1, &max);
    min_group_width(term, edit_qso_msg +10 , dlg->items +10, 1, &min);
    max_group_width(term, edit_qso_msg +11 , dlg->items +11, 1, &max);
    min_group_width(term, edit_qso_msg +11 , dlg->items +11, 1, &min);
    max_group_width(term, edit_qso_msg +12 , dlg->items +12, 1, &max);
    min_group_width(term, edit_qso_msg +12 , dlg->items +12, 1, &min);
	max_group_width(term, edit_qso_msg + 13, dlg->items + 13, 1, &max);
	min_group_width(term, edit_qso_msg + 13, dlg->items + 13, 1, &min);

    max_buttons_width(term, dlg->items +14, 2, &max);
    min_buttons_width(term, dlg->items +14, 2, &min);
    
    w = dlg->win->term->x * 9 / 10 - 2 * DIALOG_LB;
    if (w > max) w = max;
    if (w < min) w = min;
    if (w > dlg->win->term->x - 2 * DIALOG_LB) w = dlg->win->term->x - 2 * DIALOG_LB;
    if (w < 1) w = 1;
    
    rw = 0;
    y ++;
    dlg_format_group(NULL, term, edit_qso_msg + 0, dlg->items + 0, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, edit_qso_msg + 1, dlg->items + 1, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, edit_qso_msg + 2, dlg->items + 2, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, edit_qso_msg + 3, dlg->items + 3, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, edit_qso_msg + 4, dlg->items + 4, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, edit_qso_msg + 5, dlg->items + 5, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, edit_qso_msg + 6, dlg->items + 6, 1, 0, &y, w, &rw);
    dlg_format_text (NULL, term, oqsonrs, dlg->x+6, &y, w, &rw, COLOR_DIALOG_TEXT, 0);
    dlg_format_group(NULL, term, edit_qso_msg + 7, dlg->items + 7, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, edit_qso_msg + 8, dlg->items + 8, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, edit_qso_msg + 9, dlg->items + 9, 1, 0, &y, w, &rw);
    dlg_format_group(NULL, term, edit_qso_msg +10, dlg->items +10, 1, 0, &y, w, &rw);
    dlg_format_text (NULL, term, oqrg, dlg->x +6, &y, w, &rw, COLOR_DIALOG_TEXT, 0);
    dlg_format_group(NULL, term, edit_qso_msg +11, dlg->items +11, 1, 0, &y, w, &rw);
	dlg_format_group(NULL, term, edit_qso_msg + 12, dlg->items + 12, 1, 0, &y, w, &rw);
	dlg_format_group(NULL, term, edit_qso_msg + 13, dlg->items + 13, 1, 0, &y, w, &rw);
    y++;
	dlg_format_buttons(NULL, term, dlg->items + 14, 2, 0, &y, w, &rw, AL_LEFT);
    
    w = rw;
    dlg->xw = w + 2 * DIALOG_LB;
    dlg->yw = y + 2 * DIALOG_TB;

    
    center_dlg(dlg);
    draw_dlg(dlg);
    y = dlg->y + DIALOG_TB;
    y++;
    dlg_format_group(term, term, edit_qso_msg + 0, dlg->items + 0, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, edit_qso_msg + 1, dlg->items + 1, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, edit_qso_msg + 2, dlg->items + 2, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, edit_qso_msg + 3, dlg->items + 3, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, edit_qso_msg + 4, dlg->items + 4, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, edit_qso_msg + 5, dlg->items + 5, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, edit_qso_msg + 6, dlg->items + 6, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_text (term, term, oqsonrs, dlg->x+6, &y, w, &rw, COLOR_DIALOG_TEXT, 0);
    dlg_format_group(term, term, edit_qso_msg + 7, dlg->items + 7, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, edit_qso_msg + 8, dlg->items + 8, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, edit_qso_msg + 9, dlg->items + 9, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_group(term, term, edit_qso_msg +10, dlg->items +10, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    dlg_format_text (term, term, oqrg, dlg->x +6, &y, w, &rw, COLOR_DIALOG_TEXT, 0);
    dlg_format_group(term, term, edit_qso_msg +11, dlg->items +11, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
	dlg_format_group(term, term, edit_qso_msg + 12, dlg->items + 12, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
	dlg_format_group(term, term, edit_qso_msg + 13, dlg->items + 13, 1, dlg->x + DIALOG_LB, &y, w, AL_LEFT);
    y++;
    dlg_format_buttons(term, term, dlg->items +14, 2, dlg->x + DIALOG_LB, &y, w, NULL, AL_LEFT);
    
}

static int ok_dialog_stat_update(struct dialog_data *dlg, struct dialog_item_data *di)
{
    int ret;
    ret = ok_dialog(dlg,di);

    if (!ctest) return 1;
	recalc_all_stats(ctest);
    return ret;
}

void edit_qso(struct qso *qso)
{
    struct dialog *d;
    int i, len;
    static char ss[102];
    char s[1000];
    

/*    dbg("edit_qso\n");*/
    if (!qso) return;
    dump_qso(qso, "edit_qso");
    dump_all_sources(ctest);
    
    safe_strncpy0(callsign,  qso->callsign,  EQSO_LEN); z_str_uc(callsign);
    safe_strncpy0(rstr,      qso->rstr,      EQSO_LEN);
    safe_strncpy0(qsonrr,    qso->qsonrr,    EQSO_LEN);
    safe_strncpy0(exc,       qso->exc,       MAX_EXC_LEN+1); 
    safe_strncpy0(locator,   qso->locator,   EQSO_LEN); z_str_uc(locator);
    mode=qso->mode;
    safe_strncpy0(mode_str,  get_text_translation(mode_msg[mode]), EQSO_LEN);
    safe_strncpy0(rsts,      qso->rsts,      EQSO_LEN);
    safe_strncpy0(qsonrs,    qso->qsonrs,    EQSO_LEN);
    safe_strncpy0(date_str,  qso->date_str,  EQSO_LEN); 
    safe_strncpy0(time_str,  qso->time_str,  EQSO_LEN);
    safe_strncpy0(operator_, qso->operator_, EQSO_LEN);
	safe_strncpy0(remark,    qso->remark,    MAX_STR_LEN);
	LOAD_SINT(qso, phase);
    qerror = qso->error;
    qsl    = qso->qsl;
    
    safe_strncpy0(oomode_str, mode_msg[mode],EQSO_LEN);
    safe_strncpy0(oorstr,     qso->rstr,     EQSO_LEN);
    safe_strncpy0(oorsts,     qso->rsts,     EQSO_LEN);

    g_snprintf(ocallsign, OEQSO_LEN, "%s %-15s", VTEXT(T_CALLSIGN2), callsign);
    g_snprintf(orstr    , OEQSO_LEN, "%s %-15s", VTEXT(T_RSTR)     , rstr    );
    g_snprintf(oqsonrr  , OEQSO_LEN, "%s %-15s", VTEXT(T_QSONRR)   , qsonrr  );
    g_snprintf(oexc     , OEQSO_LEN, "%s %-15s", VTEXT(T_OPTEXCH)  , exc     );
    g_snprintf(olocator , OEQSO_LEN, "%s %-15s", VTEXT(T_WWL)      , locator );
    g_snprintf(omode_str, OEQSO_LEN, "%s %-15s", VTEXT(T_MODE)     , mode_str);
    g_snprintf(orsts    , OEQSO_LEN, "%s %-15s", VTEXT(T_RSTS)     , rsts    );
    g_snprintf(oqsonrs  , OEQSO_LEN, "%s %-15s", VTEXT(T_QSONRS)   , qsonrs  );
    g_snprintf(odate_str, OEQSO_LEN, "%s %-15s", VTEXT(T_DATE)     , date_str);
    g_snprintf(otime_str, OEQSO_LEN, "%s %-15s", VTEXT(T_TIME)     , time_str);
    g_snprintf(ooperator, OEQSO_LEN, "%s %-15s", VTEXT(T_OPERATOR) , operator_);
	g_snprintf(oremark,   OEQSO_LEN, "%s %-15s", VTEXT(T_REMARK), remark);
	g_snprintf(ophase_str,OEQSO_LEN, "%s %-15s", VTEXT(T_PHASE_E), phase_str);
    
    sprintf(s,"%0.0f", qso->qrg); 
    len=strlen(s);
    if (len>3) {
        memmove(s+len-2, s+len-3, 4);
        s[len-3]='.';
        if (len>6){
            memmove(s+len-5, s+len-6, 8);
            s[len-6]='.';
        }
    } 
    g_snprintf(oqrg, QRG_LEN, "%s %s Hz", VTEXT(T_QRG), s);
    
    
    if (!(d = (struct dialog*)g_malloc(sizeof(struct dialog) + 55 * sizeof(struct dialog_item)))) return;
    memset(d, 0, sizeof(struct dialog) + 55 * sizeof(struct dialog_item));
    /*d->title = "Edit QSO";*/
    g_snprintf(ss,100,VTEXT(T_EDIT_QSO_CD), aband->bandchar, atoi(qso->qsonrs));
    d->title = ss;
    d->fn = edit_qso_fn;
    d->refresh = (void (*)(void *))refresh_edit_qso;
    d->refresh_data = (void *) qso;

    
    d->items[i=0].type = D_FIELD; /* 0 */
    d->items[i].dlen = EQSO_LEN;
    d->items[i].data = callsign;
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = EQSO_LEN;
    d->items[i].data =rstr;
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = EQSO_LEN;
    d->items[i].data = qsonrr;
    
    d->items[++i].type = D_FIELD;
	d->items[i].dlen = MAX_EXC_LEN + 1;
	d->items[i].maxl = EQSO_LEN;
    d->items[i].data = exc;
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = EQSO_LEN;
    d->items[i].data = locator;

    /* 5 */
    d->items[++i].type = D_BUTTON;   
    d->items[i].gid  = 0;
    d->items[i].fn   = dlg_mode;
    d->items[i].text = mode_str;
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = EQSO_LEN;
    d->items[i].data = rsts;
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = EQSO_LEN;
    d->items[i].data = date_str;
    
    d->items[++i].type = D_FIELD;
    d->items[i].dlen = EQSO_LEN;
    d->items[i].data = time_str;
    
    d->items[++i].type = D_FIELD;  
    d->items[i].dlen = EQSO_LEN;
    d->items[i].data = operator_;
    
    d->items[++i].type = D_FIELD;    /* 10 */ 
    d->items[i].dlen = MAX_STR_LEN;
    d->items[i].maxl = EQSO_LEN;
    d->items[i].data = remark;
   
	d->items[++i].type = D_FIELD;   
	d->items[i].dlen = EQSO_LEN;
	d->items[i].data = phase_str;

	d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&qerror;
    
    d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&qsl;
    
    d->items[++i].type = D_BUTTON;      /* 14 */
    d->items[i].gid = B_ENTER;
    d->items[i].fn = ok_dialog_stat_update;
    d->items[i].text = VTEXT(T_OK);
    
    d->items[++i].type = D_BUTTON;      /* 15 */
    d->items[i].gid = B_ESC;
    d->items[i].fn = cancel_dialog;
    d->items[i].text = VTEXT(T_CANCEL);
    
    d->items[++i].type = D_END;
    do_dialog(d, getml(d, NULL));
}
                               


