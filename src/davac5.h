/*
    Tucnak - VHF contest log
    Copyright (C) 2023 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __DAVAC5_H
#define __DAVAC5_H

#include <header.h>

#ifdef HAVE_HIDAPI
int davac5_init(struct cwdaemon*);
hid_device *davac5_do_open(void);
int davac5_open(struct cwdaemon* cwda, int verbose);
int davac5_free(struct cwdaemon*);
int davac5_reset(struct cwdaemon*);
int davac5_cw(struct cwdaemon*, int onoff);
int davac5_ptt(struct cwdaemon*, int onoff);
int davac5_ssbway(struct cwdaemon*, int onoff);


void hid_info(GString* gs);

struct cm108_eeprom{
	uint16_t magic, vid, pid;
	char serial[14], product[32], manufacturer[32];
};
#define CM108_EE_SIZE 0x40

int cm108_write_u16(hid_device *handle, uint8_t addr, uint16_t data);
int cm108_read_u16(hid_device *handle, uint8_t addr, uint16_t *data);
int cm108_write_eeprom(hid_device *handle, struct cm108_eeprom *eeprom);
int cm108_read_eeprom(hid_device *handle, struct cm108_eeprom *eeprom);


#endif /* HAVE_HIDAPI */ 

#endif
