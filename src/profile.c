#include <header.h>

#if defined(__MINGW32__) && 0

int dladdr(void *addr, Dl_info *info){
    MEMORY_BASIC_INFORMATION mbi;
    char moduleName[MAX_PATH];
    HMODULE hMod;

    if (!VirtualQuery (addr, &mbi, sizeof (mbi))) return 0;

    hMod = (HMODULE)mbi.AllocationBase;

    if (!GetModuleFileNameA (hMod, moduleName, sizeof (moduleName))) return 0;

    info->file_name = (char *)(malloc (strlen (moduleName) + 1));
    strcpy (info->file_name, moduleName);

    info->base = NULL;
    info->sym_name = NULL;
    info->sym_addr = NULL;

    strcpy (info->dli_fname, module_name);
    info->dli_fbase = mbi.BaseAddress;
    // this is *probably* right
    info->dli_saddr = addr;
    strcpy (info->dli_sname, module_name);

    return 1;
}
#endif

void __cyg_profile_func_enter(void *this_fn, void *call_site)
                              __attribute__((no_instrument_function));
void __cyg_profile_func_enter(void *this_fn, void *call_site) {
    /*Dl_info dl1, dl2;
    dladdr(this_fn, &dl1);
    dladdr(__builtin_return_address(1), &dl2);
    dbg("ENTER: %s, from %s\n", dl1.dli_sname, dl2.dli_sname);*/
    dbg("ENTER: %p, from %p\n", this_fn, call_site);
}

void __cyg_profile_func_exit(void *this_fn, void *call_site)
                             __attribute__((no_instrument_function));
void __cyg_profile_func_exit(void *this_fn, void *call_site) {
    /*Dl_info dl1, dl2;
    dladdr(this_fn, &dl1);
    dladdr(__builtin_return_address(1), &dl2);*/
    dbg("EXIT:  %p, from %p\n", this_fn, call_site);
}

