/*
    Tucnak - VHF contest log
    Copyright (C) 2016-2023 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"

#include "bfu.h"
#include "cwdaemon.h"
#include "davac5.h"
#include "inputln.h"
#include "main.h"
#include "menu.h"
#include "oct8tor.h"
#include "rain.h"
#include "rc.h"
#include "rotar.h"
#include "tsdl.h"
#include "terminal.h"

int rain_enable, rain_meteox, rain_wetteronline, rain_chmi, rain_weatheronline, rain_rainviewer, rain_debug;
int rain_maxqrb, rain_minscpdist, rain_mincolor;
char rain_maxqrb_str[EQSO_LEN], rain_minscpdist_str[EQSO_LEN], rain_mincolor_str[EQSO_LEN];

char *rain_colors[EQSO_LEN] = {
	TRANSLATE("Orange 1"),
	TRANSLATE("Orange 2"),
	TRANSLATE("Orange 3"),
	TRANSLATE("Red 1"),
	TRANSLATE("Red 2"),
	TRANSLATE("White")
};


void rain_color_func(void *arg){
	int active;

	active = GPOINTER_TO_INT(arg);
	dbg("rain_color_func: active=%d", active);
	if (active<0 || active >= 14) return;
	rain_mincolor = active;
	safe_strncpy0(rain_mincolor_str, get_text_translation(rain_colors[rain_mincolor - 9]), MAX_STR_LEN);
	redraw_later();
}

int dlg_rain_color(struct dialog_data *dlg, struct dialog_item_data *di){
	int i, sel;
	struct menu_item *mi;

	if (!(mi = new_menu(1))) return 0;
	for (i = 9; i < 14; i++) {
		add_to_menu(&mi, rain_colors[i-9], "", "", MENU_FUNC rain_color_func, GINT_TO_POINTER(i), 0);
	}
	sel = rain_mincolor - 9;
	if (sel < 0) sel = 0;
	do_menu_selected(mi, GINT_TO_POINTER(rain_mincolor), sel);
	return 0;
}

void refresh_rain_opts(void *arg){
    STORE_INT(cfg, rain_enable);
    STORE_INT(cfg, rain_meteox);
    STORE_INT(cfg, rain_wetteronline);
    STORE_INT(cfg, rain_chmi);
    STORE_INT(cfg, rain_weatheronline);
	STORE_INT(cfg, rain_rainviewer);
	STORE_INT(cfg, rain_debug);

	STORE_SINT(cfg, rain_maxqrb);
	STORE_SINT(cfg, rain_minscpdist);
	STORE_INT(cfg, rain_mincolor);

#ifdef Z_HAVE_SDL    
	free_rain(grain);
	grain = init_rain();
#endif
	progress(NULL);

#ifdef AUTOSAVE
	menu_save_rc(NULL);
#endif
}

void menu_rain_opts(void *itdata, void *menudata){
    struct dialog *d;
    int i;

    LOAD_INT(cfg, rain_enable);
    LOAD_INT(cfg, rain_meteox);
    LOAD_INT(cfg, rain_wetteronline);
    LOAD_INT(cfg, rain_chmi);
	LOAD_INT(cfg, rain_weatheronline);
	LOAD_INT(cfg, rain_rainviewer);
    LOAD_INT(cfg, rain_debug);
	LOAD_SINT(cfg, rain_maxqrb);
	LOAD_SINT(cfg, rain_minscpdist);
	LOAD_INT(cfg, rain_mincolor);

	if (rain_mincolor < 9 || rain_mincolor > 14) rain_mincolor = 11;
	g_strlcpy(rain_mincolor_str, rain_colors[rain_mincolor - 9], EQSO_LEN);
	

    d = (struct dialog *)g_malloc(sizeof(struct dialog) + 20 * sizeof(struct dialog_item));
    memset(d, 0, sizeof(struct dialog) + 20 * sizeof(struct dialog_item));
    d->title = VTEXT(T_RAIN_OPTIONS); 
    d->fn = dlg_pf_fn;
    d->refresh = (void (*)(void *))refresh_rain_opts;
    d->y0 = 1;

    d->items[i = 0].type = D_CHECKBOX;      
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&rain_enable;
    d->items[i].msg = CTEXT(T_ENABLE);
    d->items[i].wrap = 2;
    
	d->items[++i].type = D_CHECKBOX;
	d->items[i].gid = 0;
	d->items[i].gnum = 1;
	d->items[i].dlen = sizeof(int);
	d->items[i].data = (char *)&rain_rainviewer;
	d->items[i].msg = "RainViewer.com (global)";
	d->items[i].wrap = 1;

	//d->items[++i].type = D_CHECKBOX;
 //   d->items[i].gid = 0;
 //   d->items[i].gnum = 1;
 //   d->items[i].dlen = sizeof(int);
 //   d->items[i].data = (char *)&rain_meteox;
 //   d->items[i].msg = "Meteox";
 //   d->items[i].wrap = 1;

 //   d->items[++i].type = D_CHECKBOX;      
 //   d->items[i].gid = 0;
 //   d->items[i].gnum = 1;
 //   d->items[i].dlen = sizeof(int);
 //   d->items[i].data = (char *)&rain_wetteronline;
 //   d->items[i].msg = "Wetteronline (West EU)";
 //   d->items[i].wrap = 1;

    d->items[++i].type = D_CHECKBOX;      
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&rain_chmi;
    d->items[i].msg = "CHMI";
    d->items[i].wrap = 1;

	d->items[++i].type = D_CHECKBOX;      
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&rain_weatheronline;
    d->items[i].msg = "Weatheronline (EU)";
    d->items[i].wrap = 1;

	
	d->items[i].wrap++;
	d->items[++i].type = D_FIELD;
	d->items[i].fn = check_number;
	d->items[i].gid = 0;
	d->items[i].gnum = 999;
	d->items[i].dlen = 5;
	d->items[i].data = (char *)&rain_maxqrb_str;
	d->items[i].msg = TRANSLATE("Max scatterpoint distance [km]:");
	d->items[i].wrap = 1;

	d->items[++i].type = D_FIELD;
	d->items[i].fn = check_number;
	d->items[i].gid = 5;
	d->items[i].gnum = 50;
	d->items[i].dlen = 5;
	d->items[i].data = (char *)&rain_minscpdist_str;
	d->items[i].msg = TRANSLATE("Min distance between SCPs [km]:");
	d->items[i].wrap = 1;

	d->items[++i].type = D_BUTTON;
	d->items[i].gid = 0;
	d->items[i].fn = dlg_rain_color;
	d->items[i].text = rain_mincolor_str;
	d->items[i].msg = TRANSLATE("Minimum SCP intensity color: ");
	d->items[i].wrap = 1;

	d->items[i].wrap++;
	d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&rain_debug;
    d->items[i].msg = "Debug images";
    d->items[i].wrap = 1;

    
//    -----------------------------
    d->items[i].wrap++;
    d->items[++i].type = D_BUTTON; 
    d->items[i].gid = B_ENTER;
    d->items[i].fn = ok_dialog;
    d->items[i].text = VTEXT(T_OK);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ESC;
    d->items[i].fn = cancel_dialog;
    d->items[i].text = VTEXT(T_CANCEL);
    d->items[i].align = AL_BUTTONS;
    d->items[i].wrap = 1;

    d->items[++i].type = D_END;
    do_dialog(d, getml(d, NULL));

}


//////// oct8tor /////////////////////////////////////////////////////////////////////////////////
#ifdef OCT8TOR

int oct_enable, oct_comm, oct_debug;
char oct_local_udp_port_str[EQSO_LEN], oct_remote_tcp_port_str[EQSO_LEN], oct_alloc_str[EQSO_LEN];
char oct_tcp_host[MAX_STR_LEN], oct_serial_port[MAX_STR_LEN];

void refresh_oct8tor_opts(void *arg){
	STORE_INT(cfg, oct_enable);
	STORE_INT(cfg, oct_comm);
	STORE_SINT(cfg, oct_local_udp_port);
	STORE_SINT(cfg, oct_remote_tcp_port);
	STORE_STR(cfg, oct_tcp_host);
	STORE_STR(cfg, oct_serial_port);
	STORE_SINT(cfg, oct_alloc);
	STORE_INT(cfg, oct_debug);

	free_rotars();
	init_rotars();
	progress(NULL);
#ifdef AUTOSAVE
	menu_save_rc(NULL);
#endif
}

void menu_oct8tor_opts(void *itdata, void *menudata){
	struct dialog *d;
	int i;

	LOAD_INT(cfg, oct_enable);
	LOAD_INT(cfg, oct_comm);
	LOAD_SINT(cfg, oct_local_udp_port);
	LOAD_SINT(cfg, oct_remote_tcp_port);
	LOAD_STR(cfg, oct_tcp_host);
	LOAD_STR(cfg, oct_serial_port);
	LOAD_SINT(cfg, oct_alloc);
	LOAD_INT(cfg, oct_debug);

	d = (struct dialog *)g_malloc(sizeof(struct dialog) + 20 * sizeof(struct dialog_item));
	memset(d, 0, sizeof(struct dialog) + 20 * sizeof(struct dialog_item));
	d->title = TRANSLATE("Oct8tor options");// VTEXT(T_oct_OPTIONS);
	d->fn = dlg_pf_fn;
	d->refresh = (void(*)(void *))refresh_oct8tor_opts;
	d->y0 = 1;

	d->items[i = 0].type = D_CHECKBOX;
	d->items[i].gid = 0;
	d->items[i].gnum = 1;
	d->items[i].dlen = sizeof(int);
	d->items[i].data = (char *)&oct_enable;
	d->items[i].msg = CTEXT(T_ENABLE);
	d->items[i].wrap = 1;


	//d->items[++i].type = D_CHECKBOX;
	//d->items[i].gid = 1;
	//d->items[i].gnum = OCT_COMM_BROADCAST;
	//d->items[i].dlen = sizeof(int);
	//d->items[i].data = (char *)&oct_comm;
	//d->items[i].msg = "Local broadcast ";

	//d->items[++i].type = D_FIELD;
	//d->items[i].fn = check_number;
	//d->items[i].gid = 0;
	//d->items[i].gnum = 65535;
	//d->items[i].dlen = 6;
	//d->items[i].data = (char *)&oct_local_udp_port_str;
	//d->items[i].msg = TRANSLATE("UDP Port:");
	//d->items[i].wrap = 1;


	d->items[++i].type = D_CHECKBOX;
	d->items[i].gid = 1;
	d->items[i].gnum = OCT_COMM_TCP;
	d->items[i].dlen = sizeof(int);
	d->items[i].data = (char *)&oct_comm;
	d->items[i].msg = "TCP/IP ";

	d->items[++i].type = D_FIELD;
	d->items[i].maxl = 20;
	d->items[i].dlen = MAX_STR_LEN;
	d->items[i].data = (char *)&oct_tcp_host;
	d->items[i].msg = TRANSLATE("Hostname:");

	d->items[++i].type = D_FIELD;
	d->items[i].fn = check_number;
	d->items[i].gid = 0;
	d->items[i].gnum = 65535;
	d->items[i].dlen = 6;
	d->items[i].data = (char *)&oct_remote_tcp_port_str;
	d->items[i].msg = TRANSLATE("Port:");
	d->items[i].wrap = 1;

	d->items[++i].type = D_CHECKBOX;
	d->items[i].gid = 1;
	d->items[i].gnum = OCT_COMM_SERIAL;
	d->items[i].dlen = sizeof(int);
	d->items[i].data = (char *)&oct_comm;
	d->items[i].msg = "Serial ";

	d->items[++i].type = D_FIELD;
	d->items[i].dlen = 6;
	d->items[i].data = (char *)&oct_serial_port;
	d->items[i].msg = TRANSLATE("Port:");
	d->items[i].wrap = 1;								  


	d->items[++i].type = D_FIELD;
	d->items[i].fn = check_number;
	d->items[i].gid = 1;
	d->items[i].gnum = 8;
	d->items[i].dlen = 6;
	d->items[i].data = (char *)&oct_alloc_str;
	d->items[i].msg = TRANSLATE("Number of rotars to alloc:");
	d->items[i].wrap = 1;


	//d->items[++i].type = D_CHECKBOX;
	//d->items[i].gid = 0;
	//d->items[i].gnum = 1;
	//d->items[i].dlen = sizeof(int);
	//d->items[i].data = (char *)&oct_debug;
	//d->items[i].msg = "Debug";
	//d->items[i].wrap = 1;


	//    -----------------------------
	d->items[i].wrap++;
	d->items[++i].type = D_BUTTON;
	d->items[i].gid = B_ENTER;
	d->items[i].fn = ok_dialog;
	d->items[i].text = VTEXT(T_OK);

	d->items[++i].type = D_BUTTON;
	d->items[i].gid = B_ESC;
	d->items[i].fn = cancel_dialog;
	d->items[i].text = VTEXT(T_CANCEL);
	d->items[i].align = AL_BUTTONS;
	d->items[i].wrap = 1;

	d->items[++i].type = D_END;
	do_dialog(d, getml(d, NULL));

}
#endif

//////// davac5  /////////////////////////////////////////////////////////////////////////////////

#ifdef HAVE_HIDAPI

char davac5_vid_str[EQSO_LEN], davac5_pid_str[EQSO_LEN];
char davac5_serial[MAX_STR_LEN], davac5_product[MAX_STR_LEN], davac5_manufacturer[MAX_STR_LEN];

void refresh_davac5_opts(void *xxx){

	hid_device *handle = davac5_do_open();
	if (handle == NULL){
		msg_box(NULL, VTEXT(T_ERROR), AL_CENTER, TRANSLATE("Can't open any Davac5 device"), NULL, 1, VTEXT(T_CANCEL), NULL, B_ENTER | B_ESC);
		return;
	}

	struct cm108_eeprom eeprom;
	
	eeprom.vid = strtol(davac5_vid_str, NULL, 16);
	eeprom.pid = strtol(davac5_pid_str, NULL, 16);
	g_strlcpy(eeprom.serial, davac5_serial, sizeof(eeprom.serial));
	g_strlcpy(eeprom.product, davac5_product, sizeof(eeprom.product));
	g_strlcpy(eeprom.manufacturer, davac5_manufacturer, sizeof(eeprom.manufacturer));

	int ret = cm108_write_eeprom(handle, &eeprom);
	hid_close(handle);
	if (ret < 0){
		msg_box(NULL, VTEXT(T_ERROR), AL_CENTER, TRANSLATE("Can't write Davac5 EEPROM"), NULL, 1, VTEXT(T_CANCEL), NULL, B_ENTER | B_ESC);
		return;
	}
}

void davac5_opts(void *arg)
{ 
    struct dialog *d;
    int i;
	
	hid_device *handle = davac5_do_open();
	if (handle == NULL){
		msg_box(NULL, VTEXT(T_ERROR), AL_CENTER, TRANSLATE("Can't open any Davac5 device"), NULL, 1, VTEXT(T_CANCEL), NULL, B_ENTER | B_ESC);
		return;
	}

	struct cm108_eeprom eeprom;
	int ret = cm108_read_eeprom(handle, &eeprom);
	hid_close(handle);
	if (ret == -5){ // bad magic
		g_snprintf(davac5_vid_str, sizeof(davac5_vid_str), "0x%04x", 0x0d8c);
		g_snprintf(davac5_pid_str, sizeof(davac5_pid_str), "0x%04x", 0x0012);
		g_strlcpy(davac5_serial, "#0001", sizeof(davac5_serial));
		g_strlcpy(davac5_product, "Davac 5.0", sizeof(davac5_product));
		g_strlcpy(davac5_manufacturer, "OK1ZIA", sizeof(davac5_manufacturer));
	}else if (ret < 0){
		msg_box(NULL, VTEXT(T_ERROR), AL_CENTER, TRANSLATE("Can't read Davac5 EEPROM"), NULL, 1, VTEXT(T_CANCEL), NULL, B_ENTER | B_ESC);
		return;
	}else{
		g_snprintf(davac5_vid_str, sizeof(davac5_vid_str), "0x%04x", eeprom.vid);
		g_snprintf(davac5_pid_str, sizeof(davac5_pid_str), "0x%04x", eeprom.pid);
		g_strlcpy(davac5_serial, eeprom.serial, sizeof(davac5_serial));
		g_strlcpy(davac5_product, eeprom.product, sizeof(davac5_product));
		g_strlcpy(davac5_manufacturer, eeprom.manufacturer, sizeof(davac5_manufacturer));
	}


	d = (struct dialog *)g_malloc(sizeof(struct dialog) + 20 * sizeof(struct dialog_item));
	memset(d, 0, sizeof(struct dialog) + 20 * sizeof(struct dialog_item));
	d->title = TRANSLATE("Davac5 EEPROM");// VTEXT(T_oct_OPTIONS);
	d->fn = dlg_pf_fn;
	d->refresh = (void(*)(void *))refresh_davac5_opts;
	d->y0 = 1;

	d->items[i = 0].type = D_FIELD;
	d->items[i].dlen = EQSO_LEN;
	d->items[i].maxl = 8;
	d->items[i].data = (char *)&davac5_vid_str;
	d->items[i].msg = TRANSLATE("VID:");//CTEXT(T_ENABLE);
	d->items[i].wrap = 0;

	d->items[++i].type = D_FIELD;
	d->items[i].dlen = EQSO_LEN;
	d->items[i].maxl = 8;
	d->items[i].data = (char *)&davac5_pid_str;
	d->items[i].msg = TRANSLATE("PID:");//CTEXT(T_ENABLE);
	d->items[i].wrap = 1;

	d->items[++i].type = D_FIELD;
	d->items[i].dlen = sizeof(eeprom.manufacturer);
	d->items[i].maxl = 32;
	d->items[i].data = (char *)&davac5_manufacturer;
	d->items[i].msg = TRANSLATE("Manufacturer:");//CTEXT(T_ENABLE);
	d->items[i].wrap = 1;

	d->items[++i].type = D_FIELD;
	d->items[i].dlen = sizeof(eeprom.product);
	d->items[i].maxl = 32;
	d->items[i].data = (char *)&davac5_product;
	d->items[i].msg = TRANSLATE("     Product:");//CTEXT(T_ENABLE);
	d->items[i].wrap = 1;

	d->items[++i].type = D_FIELD;
	d->items[i].dlen = sizeof(eeprom.serial);
	d->items[i].maxl = 14;
	d->items[i].data = (char *)&davac5_serial;
	d->items[i].msg = TRANSLATE("      Serial:");//CTEXT(T_ENABLE);
	d->items[i].wrap = 1;

	//    -----------------------------
	d->items[i].wrap++;
	d->items[++i].type = D_BUTTON;
	d->items[i].gid = B_ENTER;
	d->items[i].fn = ok_dialog;
	d->items[i].text = VTEXT(T_OK);

	d->items[++i].type = D_BUTTON;
	d->items[i].gid = B_ESC;
	d->items[i].fn = cancel_dialog;
	d->items[i].text = VTEXT(T_CANCEL);
	d->items[i].align = AL_BUTTONS;
	d->items[i].wrap = 1;

	d->items[++i].type = D_END;
	do_dialog(d, getml(d, NULL));

	if (ret == -5) { // magic
		msg_box(NULL, VTEXT(T_ERROR), AL_CENTER, TRANSLATE("Invalid EEPROM data, reverting to default"), NULL, 1, VTEXT(T_CANCEL), NULL, B_ENTER | B_ESC);

	}

}

int dlg_davac5_opts(struct dialog_data *dd, struct dialog_item_data *did){
    davac5_opts(NULL);
    return 0;
}

#endif