/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __PLAYER_H
#define __PLAYER_H

#include "header.h"

struct subwin;
struct event;

#define PREVIEW_H 3
#define PLAYER_H 1

int  sw_player_kbd_func(struct subwin *sw, struct event *ev, int fw);
int  sw_player_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_player_redraw(struct subwin *sw, struct band *band, int flags);
void sw_player_check_bounds(struct subwin *sw);
void sw_player_raise(struct subwin *sw);

void sw_player_free(struct subwin *sw);
void player_play(char *filename);


#endif
