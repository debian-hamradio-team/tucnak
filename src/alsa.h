/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ALSA_H
#define __ALSA_H

#include "dsp.h"

#ifdef HAVE_ALSA

#ifdef HAVE_SNDFILE
int alsa_set_format(struct dsp *dsp, SF_INFO *sfinfo, int rec);
#endif
int alsa_open(struct dsp *dsp, int rec);
int alsa_close(struct dsp *dsp);
int alsa_write(struct dsp *dsp, void *data, int frames);
int alsa_read(struct dsp *dsp, void *data, int frames);
int alsa_reset(struct dsp *dsp);
int alsa_sync(struct dsp *dsp);
int alsa_get_sources(struct dsp *dsp, GString *labels);
int alsa_set_source(struct dsp *dsp);
int alsa_set_plevel(struct dsp *dsp);
void alsa_info(GString *gs);
char *alsa_get_state(struct dsp *dsp);
int alsa_set_sdr_format(struct dsp *dsp, int frames, int speed, int rec);

#endif
#endif
