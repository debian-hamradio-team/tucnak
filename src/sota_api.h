/*
    Tucnak - SOTA SSO and Network API
    Copyright (C) 2002-2020  Ladislav Vaiz <ok1zia@nagano.cz>;
    2020 Michal OK2MUF

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SOTA_API_H
#define __SOTA_API_H

enum sota_token_state
{
    SOTA_SSO_VALID,
    SOTA_SSO_EXPIRED,
    SOTA_SSO_LOGOUT,
    SOTA_SSO_ERROR,
};

struct sota_api_data
{
    struct zhttp *http;
    struct zhttp *http_spot;
    char *status;
    char *error;
    char *error_description;
    char *id_token;
    char *access_token;
    uint32_t ts_token_expire;
    enum sota_token_state state;
    struct sota_spot_data *ssd;
};

struct sota_api_data* sota_api_init(void);
int sota_api_get_token(struct sota_api_data *sad);
int sota_api_send_spot(struct sota_api_data *sad);
int sota_api_send_spot_insecure(struct sota_api_data *sad);
void sota_api_free(struct sota_api_data *sad);

#endif