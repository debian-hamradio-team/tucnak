/*
    oct8tor.h - Oct8tor support
    Copyright (C) 2023 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __OCT8TOR_H
#define __OCT8TOR_H

#include <libzia.h>

#ifdef OCT8TOR

enum oct_comm{
	OCT_COMM_BROADCAST,
	OCT_COMM_TCP,
	OCT_COMM_SERIAL,
	OCT_COMM_CAN
};

struct oct8tor{
	enum oct_comm comm;
	struct zserial *zser;
	int zser_fd;
	struct zbinbuf *rdbuf, *wrbuf;

	GThread *thread;
	int thread_break;
	int poll_ms;
	int keepalive;

	bool onconnect;
	char *adifband;
};

struct oct8tor *init_oct8tor(void);
void free_oct8tor(struct oct8tor *oct);

// worker thread
gpointer oct8tor_main(gpointer xxx);
void oct8tor_read(struct oct8tor *oct);
void oct8tor_handle_error(struct oct8tor* oct, bool isErrror);

// main thread
void oct8tor_read_handler(int n, char **line);
void oct8tor_onreceived(struct oct8tor *oct, struct zjson *line);

void oct8tor_rcvd_rotars(struct oct8tor *oct, struct zjson *json);
void oct8tor_rcvd_angle(struct oct8tor *oct, struct zjson *json);


void oct8tor_send_band(struct oct8tor *oct8tor, const char *adifband);


#endif
#endif
