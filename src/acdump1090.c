/*
    acdump1090.c - airscatter.dk provider for AS
    Copyright 2024 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.
*/

#include "ac.h"
#include <libzia.h>
#include "header.h"
#include "fifo.h"
#include "main.h"
#include "tsdl.h"

// nap�. https://sq6emm.hamradio.pl/adsb/

int acs_dump1090_init(struct acs* acs) {
    if (!cfg->ac_dump1090_url || !*cfg->ac_dump1090_url) {
        log_adds(TRANSLATE("Please set the dump1090 URL in Aicraft scatter options"));
        return -1;
    }


    acs->dump1090_http_timer = zselect_timer_new(zsel, 1000, acs_dump1090_http_timer, acs);

    acs->load_from_main_thread = acs_dump1090_load_from_main_thread;
#ifdef Z_HAVE_SDL
    acs->plot_info = acs_dump1090_plot_info;
#endif
    acs->dead_minutes = 4;
    return 0;
}

void acs_dump1090_free(struct acs* acs) {
    if (acs->dump1090_http_timer) zselect_timer_kill(zsel, acs->dump1090_http_timer);
    zhttp_free(acs->dump1090_http);
    g_free(acs->dump1090_http_error);
}

void acs_dump1090_http_timer(void* arg) {
    struct acs* acs = (struct acs*)arg;
    acs->dump1090_http_timer = 0;

    time_t now = time(NULL);
    struct tm utc;
    gmtime_r(&now, &utc);

    dbg("\n%02d:%02dz acs_dump1090_http_timer\n", utc.tm_hour, utc.tm_min);
    //const char* mywwl = ctest ? ctest->pwwlo : cfg->pwwlo;
    //if (cfg->ac_centerwwl != NULL && strlen(cfg->ac_centerwwl) >= 4) mywwl = cfg->ac_centerwwl;
    //double lng = qth(mywwl, 0) * 180.0 / M_PI;
    //double lat = qth(mywwl, 1) * 180.0 / M_PI;

    char* url = g_strdup_printf("%s?_=%lld", cfg->ac_dump1090_url, (long long)time(NULL));;
    dbg("dump1090 url=%s\n", url);

    acs->dump1090_http = zhttp_init();
    zhttp_get(acs->dump1090_http, zsel, url, acs_dump1090_downloaded_callback, acs);
    g_free(url);
}

void acs_dump1090_downloaded_callback(struct zhttp* http) {
    char* data;
    int remains;
    struct acs* acs = (struct acs*)http->arg;

    //dbg("acs_dump1090_downloaded_callback\n");

    if (http->state == ZHTTPST_ERROR && http->errorstr) {
        dbg("ERROR acs_dump1090_downloaded_callback: %s\n", http->errorstr);
        g_free(acs->dump1090_http_error);
        acs->dump1090_http_error = g_strdup(http->errorstr);
        zhttp_free(http);
        acs->dump1090_http = NULL;
    }
    else {
        if (http_is_content_type(http, "application/json")) {
            char* content_length = http_get_header(http, "Content-Length");
            dbg("OK acs_dump1090_downloaded_callback  Content-Length=%s   inflated length=%d\n", content_length != NULL ? content_length : "???", http->response->len - http->dataofs);
            data = g_new(char, http->response->len + 1);
            zbinbuf_getstr(http->response, http->dataofs, data, http->response->len + 1);



            zhttp_free(http);
            acs->dump1090_http = NULL;

            MUTEX_LOCK(acs->sh);
            g_free(acs->sh.data);
            acs->sh.data = data;
            MUTEX_UNLOCK(acs->sh);

            zg_free0(acs->dump1090_http_error);

            remains = /*TODOcfg->ac_dump1090_minutes*/ 1 * 60000;
            acs->dump1090_http_timer = zselect_timer_new(zsel, remains, acs_dump1090_http_timer, acs);
            return;
        }
        else {
            dbg("ERROR acs_dump1090_downloaded_callback: Bad Content-Type\n");
            g_free(acs->dump1090_http_error);
            acs->dump1090_http_error = g_strdup("Bad Content-Type");

            zhttp_free(http);
            acs->dump1090_http = NULL;
        }
    }

    remains = 60000; // if error retry after 1 minute (longer, maybe server can decrease remaining if reply failed in the middle)
    acs->dump1090_http_timer = zselect_timer_new(zsel, remains, acs_dump1090_http_timer, acs);

}

void acs_dump1090_load_from_main_thread(struct acs* acs, const char* data) {
    struct zjson* json = zjson_init(data);
    struct zjson* aircraft = zjson_get_array(json, "aircraft");
    if (aircraft != NULL) {
        int n = 0, added = 0;
        while (1) {
            struct zjson* one = zjson_get_object(aircraft, NULL);
            if (one == NULL) break;

            n++;

            int alt_baro = zjson_get_int(one, "alt_baro", 0);
            if (alt_baro == 0) alt_baro = zjson_get_int(one, "altitude", 0);
            int asl = alt_baro * 0.3048;
            if (asl < cfg->ac_minalt) {
                zjson_free(one);
                continue;
            }

            double lat = zjson_get_double(one, "lat", NAN) * M_PI / 180.0;
            double lon = zjson_get_double(one, "lon", NAN) * M_PI / 180.0;
            if (isnan(lat) || isnan(lon)) {
                zjson_free(one);
                continue;
            }

            char* hex = zjson_get_str(one, "hex", NULL);
            if (hex == NULL) {
                zjson_free(one);
                continue;
            }

            struct ac* ac = g_new0(struct ac, 1);
            g_strlcpy(ac->id, hex, AC_ID_MAXLEN);
            g_free(hex);

            ac->w = lat;
            ac->h = lon;

            ac->qtfdir = (zjson_get_int(one, "track", 0) - 90) * M_PI / 180.0;

            ac->asl = asl;
            ac->feets = alt_baro;

            int gs = zjson_get_int(one, "gs", 0);
            if (gs == 0) gs = zjson_get_int(one, "speed", 0);

            ac->knots = gs;
            ac->speed = ac->knots * 1.852;


   /*         char* callsign = zjson_get_str(one, "Call", NULL);
            if (callsign != NULL) {
                g_strlcpy(ac->callsign, callsign, AC_CALLSIGN_MAXLEN);
                g_free(callsign);
            }*/

           /* double PosTime = zjson_get_double(one, "PosTime", 0.0);
            ac->when = (int)(PosTime / 1000.0);*/
            ac->when = time(NULL);

            /*char* type = zjson_get_str(one, "Type", NULL);
            if (type != NULL) {
                g_strlcpy(ac->icaotype, type, AC_ICAOTYPE_MAXLEN);
                g_free(type);
            }*/

            ac_fill_wingspan(acs, ac);

            g_ptr_array_add(acs->th.acs, ac);
            added++;
            zjson_free(one);
        }

        dbg("downloaded %d, added %d\n", n, added);
    }
    zjson_free(aircraft);
    zjson_free(json);
}

#ifdef Z_HAVE_SDL
void acs_dump1090_plot_info(struct subwin* sw, SDL_Rect* area, struct acs* acs) {
    SDL_Surface* surface = sdl->screen;
    int c;

    if (acs->dump1090_http_error != NULL) {
        c = z_makecol(252, 0, 0);
        zsdl_printf(surface, area->x + 50, area->y + 5, c, 0, ZFONT_TRANSP | ZFONT_OUTLINE, TRANSLATE("dump1090 error: %s"), acs->dump1090_http_error);
        return;
    }

    zsdl_printf(surface, area->x + 50, area->y + 5, sdl->gr[12], 0, ZFONT_TRANSP | ZFONT_OUTLINE, "dump1090");

}
#endif