/*
    acosn.c - firefox extension (obsolete)
    Copyright 2023 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.
*/

#include "ac.h"
#include <libzia.h>
#include "header.h"
#include "fifo.h"

int acs_file_init(struct acs* acs) {

    if (!cfg->ac_filename || !*cfg->ac_filename) {
        log_adds(TRANSLATE("Please set Feed filename in Aicraft scatter options"));
        return -1;
    }

    acs->load_from_thread = acs_file_read_feed;

    return 0;
}

void acs_file_free(struct acs* acs) {

}

//for flightradar
static int ac_icon2wingspan[] = {
    115, // 0: B737
    0,   // 1: fighter
    0,   // 2: ultralight
    0,   // 3: too small
    0,   // 4: too small
    52,  // 5: small
    75,  // 6: ATR, Beech
    114, // 7: A319, Embraer
    82,  // 8: CL-600
    167, // 9: C-17A air force
    114, // 10: A320
    118, // 11: A320, B757
    154, // 12: B767
    197, // 13: A330-743L
    170, // 14: MD-11F
    196, // 15: B767, A330
    180, // 16: A340
    206, // 17: A340
    203, // 18: B777
    223, // 19: B747
    246, // 20: A380
    0,   // 21: ?
    0,   // 22: ?
    0,   // 23: copters
    0,   // 24: baloons
};


void acs_file_read_feed(struct acs* acs) {
    ST_START();
    MUTEX_LOCK(acs->sh);
    if (access(acs->sh.filename, 0) < 0) {
        MUTEX_UNLOCK(acs->sh);
        return;
    }
    char* tmp = g_strconcat(acs->sh.filename, ".tmp", NULL);
    unlink(tmp);
    rename(acs->sh.filename, tmp);
    const char* content = zfile_read_textfile(tmp);
    g_free(tmp);
    MUTEX_UNLOCK(acs->sh);

    if (!content) return;
     

    acs->th.acs = g_ptr_array_new();


    struct zjson* array = zjson_init(content);
    int n = 0, added = 0;
    while (1) {
        struct zjson* one = zjson_get_array(array, NULL);
        if (one == NULL) break;

        n++;

        int feets = zjson_get_int(one, "feets", 0);
        int asl = feets * 0.3048;
        if (asl < cfg->ac_minalt) {
            zjson_free(one);
            continue;
        }

        char* id = zjson_get_str(one, "id", NULL);
        if (id == NULL) continue;


        double lat = zjson_get_double(one, "lat", NAN) * M_PI / 180.0;
        double lon = zjson_get_double(one, "lon", NAN) * M_PI / 180.0;

        double qrb, qtf; 
        hw2qrbqtf(acs->th.myh, acs->th.myw, lon, lat, &qrb, &qtf);
        if (qrb > 600) {    
            g_free(id);
            zjson_free(one);
            continue;
        }

        struct ac* ac = g_new0(struct ac, 1);
        if (id != NULL) {
            g_strlcpy(ac->id, id, AC_ID_MAXLEN);
            g_free(id);
        }
        ac->w = lat;
        ac->h = lon;

        ac->qtfdir = (zjson_get_int(one, "qtf", 0) - 90) * M_PI / 180.0;

        ac->feets = zjson_get_int(one, "feets", 0);
        ac->asl = ac->feets * 0.3048;

        ac->knots = zjson_get_int(one, "knots", 0);
        ac->speed = ac->knots * 1.852;

        char *callsign = zjson_get_str(one, "callsign", NULL);
        if (callsign != NULL) {
            g_strlcpy(ac->callsign, callsign, AC_CALLSIGN_MAXLEN);
            g_free(callsign);
        }

        ac->icon = zjson_get_int(one, "icon", -1);
        if (ac->icon >= 0 && ac->icon < sizeof(ac_icon2wingspan) / sizeof(int)) { // default wingspan is 0
            ac->wingspan = ac_icon2wingspan[ac->icon];
        }
        ac->when = (time_t)zjson_get_long(one, "when", 0);

        char *type = zjson_get_str(one, "type", NULL);
        if (type != NULL) {
            g_strlcpy(ac->icaotype, type, AC_ICAOTYPE_MAXLEN);
            g_free(type);
        }

        ac_fill_wingspan(acs, ac);

        g_ptr_array_add(acs->th.acs, ac);
        added++;
        zjson_free(one);
    }
    zjson_free(array);
    dbg("readed %d, added %d\n", n, added);
    acs->validity = time(NULL) + 10 * 60;

    MUTEX_LOCK(acs->sh);
    zg_ptr_array_free_all(acs->sh.acs);
    acs->sh.acs = acs->th.acs;
    acs->th.acs = NULL;
    MUTEX_UNLOCK(acs->sh);
}

