/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __HTML_H
#define __HTML_H

#include "header.h"
struct band;
struct config_band;
struct subwin;

#define HTML_ICON 0x01
#define HTML_MAP 0x02
#define HTML_CHART 0x04
#define HTML_IMG_ROOT 0x08
#define HTML_FOR_PHOTO 0x10
#define HTML_CENTER 0x20

gchar *qh(GString *gs, gchar *s);
void html_style(GString *gs);
void html_header(GString *gs, gchar *title, int flags, int refresh, char *bodyarg);
void html_ref_header(GString *gs);
void html_band_ref(GString *gs, struct band *b, struct config_band *confb);
void html_total_sum(GString *gs, struct band *b, struct config_band *confb);
void html_ref_footer(GString *gs, struct band *b, struct config_band *confb);
void html_band_header(GString *gs, struct band *b, struct config_band *confb, int flags, struct subwin *map, struct subwin *chart);
void html_band_footer(GString *gs);
void html_footer(GString *gs, int flags);
void html_qsos(GString *gs, struct band *b, struct config_band *confb);
void html_complete(GString *gs, int flags);

int export_all_bands_html(void);

#endif
