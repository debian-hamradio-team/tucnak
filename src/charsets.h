/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __CHARSETS_H
#define __CHARSETS_H

struct conv_table {
    int t;
    union {
        char *str;
        struct conv_table *tbl;
    } u;
};

/*struct conv_table *get_translation_table(int, int);*/
int get_cp_index(char *n);
char *get_cp_name(int);
/*char *get_cp_mime_name(int);*/
int is_cp_special(int);
void free_conv_table(void);
char *cp2utf_8(int, int);
char *u2cp(int, int);


#endif
