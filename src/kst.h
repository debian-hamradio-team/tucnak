/*
    kst.h ON4KST chat
    Copyright (C) 2011-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __KST_H
#define __KST_H

#include "header.h"
 
struct subwin;
struct event;
struct zasyncdns;
struct fifo;
struct band;
union zsockaddr;
struct qrv_item;

int sw_kst_kbd_func(struct subwin *sw, struct event *ev, int fw);
int sw_kst_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_kst_redraw(struct subwin *sw, struct band *band, int flags);
void sw_kst_check_bounds(struct subwin *sw);

void sw_kst_enter(void *enterdata, gchar *str, int cq);
void sw_kst_addrinfo(struct zasyncdns *adns, int n, int *family, int *socktype, int *protocol, int *addrlen, union zsockaddr *addr, char *errorstr);
void sw_kst_connected_handler(void *xxx);
void sw_kst_read_handler(void *xxx);
void sw_kst_disconnect(struct subwin *sw);

char *sw_kst_call_under(struct subwin *sw, int x, int y);
void sw_kst_toggle_highlight(struct subwin *sw, char *call, int val);
int sw_kst_line_color(struct subwin *sw, char *line);
char *sw_kst_counterp(struct subwin *sw, char *line, int *color); // thread unsafe!
int sw_kst_color(char *call);

void sw_kst_nothing(void *itdata, void *menudata);
void sw_kst_select(void *itdata, void *menudata);
void sw_kst_hide(void *itdata, void *menudata);
void sw_kst_message(void *itdata, void *menudata);
void sw_kst_ask_sked(void *itdata, void *menudata);
void sw_kst_info(void *itdata, void *menudata);
#ifdef Z_HAVE_SDL
void sw_kst_ac_info(void *itdata, void *menudata);
#endif
void sw_kst_use(void *itdata, void *menudata);
int kst_read_line(struct subwin *sw, char *line);

void kst_timer_function(void *data);
void kst_end_shus(struct subwin *sw);
void kst_clear_users(struct subwin *sw);
int kst_user_skip(struct subwin *sw, const char *call);
int kst_qrb_skip(struct qrv_item *qi, char *call);
int kst_wkdwwl_skip(struct qrv_item *qi);
void kst_shus(struct subwin *sw, int shus);
void sw_kst_raise(struct subwin *sw);

void kst_open_connection(void *itdata, void *menudata);
void kst_export_text(void *itdata, void *menudata);

#ifdef Z_HAVE_SDL    
void kst_free_screen(struct subwin *sw);
#endif
void kst_open_bh(void *sw);
void kst_dump_skip(void *x, void *menudata);
GPtrArray* sw_kst_calls_under(struct subwin *sw, int n, int y);
void kst_right_mouse_2(struct subwin *sw);
void sw_kst_call_choosen(void *itdata, void *menudata);
void sw_kst_write_handler(void *arg);

#endif
