/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __EXCDB_H
#define __EXCDB_H

#include "header.h"
#include "qsodb.h"

extern struct excdb *excdb;  
  
struct exc_item{
   gchar *exc0, *exc1;    
   gint stamp0, stamp1;
};

struct cxe_item{
   gchar *call0, *call1;    
   gint stamp0, stamp1;
};

struct excdb{
    GHashTable *exc;  /* key=call, value=exc_item */
    GHashTable *cxe;  /* key=exc, value=cxe_item */
    GHashTable *vexc; /* key=exc, value=null (exists or not) */
    ZPtrArray *vexcia; /* sorted excs */
    int latest;
    gchar *excname;
    enum exctype exctype;
};


struct excdb *init_exc(void);
gboolean free_exc_item(gpointer key, gpointer value, gpointer user_data);
gboolean free_cxe_item(gpointer key, gpointer value, gpointer user_data);
gboolean free_vexc_item(gpointer key, gpointer value, gpointer user_data);
void free_exc(struct excdb *excdb);
void clear_exc(struct excdb *excdb);

gint get_exc_size(struct excdb *excdb);
gint get_cxe_size(struct excdb *excdb);
gint get_vexc_size(struct excdb *excdb);

void load_one_exc(struct excdb *excdb, gchar *s);
int load_exc_from_file(struct excdb *excdb, gchar *filename);

void load_one_vexc(struct excdb *excdb, gchar *s);
int load_vexc_from_file(struct excdb *excdb, gchar *filename);
int load_vexc_from_mem(struct excdb *excdb, const char *file, const long int len);

int is_valid_vexc(struct excdb *excdb, gchar *vexc);
    
void read_exc_files(struct excdb *excdb, enum exctype exctype, gchar *excname);
void exc_ia_verified(gpointer key, gpointer value, gpointer user_data);
void exc_ia_waz(gpointer key, gpointer value, gpointer user_data);
void exc_ia_itu(gpointer key, gpointer value, gpointer user_data);

void save_one_exc(gpointer key, gpointer value, gpointer user_data);
int save_exc_string(struct excdb *excdb, GString *gs);
int save_exc_into_file(struct excdb *excdb, gchar *filename);

void add_exc(struct excdb *excdb, gchar *call, gchar *wwl, gint stamp);
void add_cxe(struct excdb *excdb, gchar *wwl, gchar *call, gint stamp);

gchar *find_exc_by_call(struct excdb *excdb, const gchar *call);
gchar *find_exc_by_call_newer(struct excdb *excdb, gchar *call, int maxstamp);
gchar *find_call_by_exc(struct excdb *exc, gchar *wwl);
void update_exc_from_band(struct excdb *excdb, struct band *band);
void update_exc_from_ctest(struct excdb *excdb, struct contest *ctest);
int valid_vexc(struct excdb *excdb, gchar *vexc);
int rsgbdc_maxcount(char *exc);
void usaca_multiple_exc(char *s);


#endif
