#!/usr/bin/perl -w

open(F, "tucnakdw") or die;
open(G, ">tucnakdw.tmp") or die;

while ($s=<F>){
    $s=~s/[\r\n]+//;
    if ($s=~/^#/) {
        print G "$s\n";
        next;
    }
    if ($s=~/^\s*$/) {
        print G "$s\n";
        next;
    }
    print G "$s\n";
    
    $s=<F>;
    $s=~s/[\r\n]+//;
    die "empty second line" if (!defined($s));
    @a=split(/\s+/, $s);
    %a=();
    foreach $a (@a) {$a{$a}=1;}
    @a=sort(keys(%a));
    $s=join(" ", @a);
    print G "$s\n";
    
    $s=<F>;         #can be empty string
    if (defined($s)){
        $s=~s/[\r\n]+//;
        print G "$s\n";
    }
    
}

close(F);
close(G);

rename("tucnakdw.tmp", "tucnakdw");
