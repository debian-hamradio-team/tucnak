#!/bin/sh

echo "*** This is upgrade script for Tucnak"
echo "*** Please follow instructions below"
echo ""

echo "sudo LANG= LC_ALL= dpkg -i '$1' '$2'"
LANG= LC_ALL= sudo dpkg -i "$1" "$2"
if test x"$?" = x"0" ; then
    echo "*** Seems OK. Please restart Tucnak"
    exit 0
else
    echo "*** sudo returns $?, falling to su"
fi


echo "*** Please enter root's password"
echo su -l -c dpkg -i "$1" "$2" root
su -l -c "dpkg -i \"$1\" \"$2\"" root
if test x"$?" = x"0" ; then
    echo "*** Seems OK. Please restart Tucnak"
    exit 0
else
    echo "*** su returns $?, giving up"
    echo "*** Please run as root: dpkg -i '$1' '$2'"
    exit 1
fi

