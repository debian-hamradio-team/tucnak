/*
    zdump.h - make core dump
    Copyright (C) 2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __Z_DUMP_H
#define __Z_DUMP_H

#include <libziaint.h>

#include <glib.h>

// can be called more times
void z_dump_init(char *filename, void (*handler)(GString *gs), char *msg_title, char *appddir);

void z_dump_free(void);		   
void z_dump_create(void);
char *z_dump_backtrace(GString *gs, void *vex, void *ctx, int level);



#endif
