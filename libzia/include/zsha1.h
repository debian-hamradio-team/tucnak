#ifndef __ZSHA1_H
#define __ZSHA1_H

#include <libziaint.h>

/*
   SHA-1 in C
   By Steve Reid <steve@edmweb.com>
   100% Public Domain
 */

#include "stdint.h"

typedef struct
{
    uint32_t state[5];
    uint32_t count[2];
    unsigned char buffer[64];
} zsha1_ctx;

void zsha1_transform(uint32_t state[5], const unsigned char buffer[64]);
void zsha1_init(zsha1_ctx * context);
void zsha1_update(zsha1_ctx * context, const unsigned char *data, uint32_t len);
void zsha1_final(unsigned char digest[20], zsha1_ctx * context);
void zsha1(char *hash_out, const char *str, int len);

#endif /* SHA1_H */
