/*
    Misc functions
    Copyright (C) 2012-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZMISC_H
#define __ZMISC_H

#include <libziaint.h>
#include <glib.h>

#ifdef Z_HAVE_TERMIOS
#ifdef Z_CYGWIN
void cfmakeraw (struct termios *);
#endif
#endif

#ifdef Z_MSC_MINGW
#include <windows.h>
#include <process.h>
#endif


char *z_strdup_home(void);

#ifdef Z_MSC_MINGW
int setenv(const char *name, const char *value, int overwrite);
#endif

char *z_username(char *buf, int buflen);
void z_get_settings(GString *gs);

double z_min3_d(double a, double b, double c);
double z_max3_d(double a, double b, double c);

static inline double z_sqr(double x){
	return x * x;
}

#ifdef Z_MSC_MINGW
#define z_finite(a) _finite(a)
#else
#define z_finite(a) isfinite(a)
#endif

// argument is fftw_complex
#define Z_CABS(x) (sqrt( (x)[0]*(x)[0]  + (x)[1]*(x)[1]))

// arguments are fftw_complex
#define Z_CMUL(out, x, y) {\
	double r = (x)[0] * (y)[0] - (x)[1] * (y)[1]; \
    (out)[1] = (x)[1] * (y)[0] + (x)[0] * (y)[1]; \
	(out)[0] = r; \
}



#ifdef Z_MSC_MINGW
int zreg_getint(HKEY hSecKey, const char *section, const char *entry);
char *zreg_getstr(HKEY hSecKey, const char *section, const char *entry);
#endif

int z_browser(char *url);

#define Z_CLR 0
#define Z_SET 1
#define Z_XOR 2


#if _MSC_VER < 1700 /* 2012 */
double round(double number);
#endif

#ifdef Z_MSC_MINGW
#define getpid _getpid
#endif

#ifdef Z_MSC
typedef int ssize_t;
#endif


#endif
