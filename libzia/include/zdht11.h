/*
    dht11.h - skeleton for new header files
    Copyright (C) 2019 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZDHT11_H
#define __ZDHT11_H

struct zgpio;

struct zdht11{
    float temperature, humidity;
};

int zdht11_read_once(struct zdht11 *dht, struct zgpio *gpio, int prot);
int zdht11_read(struct zdht11 *dht, struct zgpio *gpio, int prot);


#endif
