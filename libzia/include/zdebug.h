
/*
    zdebug.h - debugging functions
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZDEBUG_H
#define __ZDEBUG_H

#include <libziaint.h>

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

#include <glib.h>

extern char *debug_msg_title;

void zdebug_init(int argc, char *argv[], void (*free_callback)(void), void (*trace_callback)(char *), char *msg_title);
void zdebug_free(void);
void z_g_log_func(const gchar *log_domain, GLogLevelFlags log_level, const gchar *message, gpointer user_data);
void z_alsa_error_handler(const char *file, int line, const char *function, int err, const char *fmt, ...);
int zdebug_debug_type(void);

void dbg(char *m, ...);
void error(char *m, ...);
void trace(int enable, char *m, ...);

void zinternal_error(char *file, int line, char *, ...);

#ifdef __GNUC__
#define zinternal(m...) zinternal_error(__FILE__, __LINE__, m)
#else
#define zinternal(m, ...) zinternal_error(__FILE__, __LINE__, m, __VA_ARGS__)
#endif

#ifdef __GNUC__

static inline void zforce_dump(void)
{
#ifdef Z_ANDROID
    error("%s\n", "Forcing core dump");
#else
    error("\n\033[1m%s\033[0m\n", "Forcing core dump");
#endif
//    fflush(stderr);
    raise(SIGSEGV);
    exit(1);
}
#endif

void zhexdump(void *buf, size_t len, const char *desc);

#endif
