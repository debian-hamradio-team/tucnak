/*
    svnver - keeps and reports SVN revision number
    Copyright (C) 2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZVER_H
#define __ZVER_H

// exports libzia svn version to application
const char *z_svnver(void);


#endif
