/*
    zpath.h - path functions
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZPATH_H
#define __ZPATH_H

#include <libziaint.h>

#ifdef Z_MSC_MINGW
char *z_wokna(char *file);
#else
static inline char *z_wokna(char *file) { return file; }
#endif
char *z_unix(char *file);

char *z_dirname(char *filename);
const char *z_filename(const char *filename);
const char *z_extension(const char *filename);
char *z_optimize_path(char *src); /* src must be allocated by glib, return can differ */
char *z_binary_file_name(void);
char *z_libzia_file_name(void **codebase);


#endif
