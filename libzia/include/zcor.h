/*
    zcor.c - Polar map data
    Copyright (C) 2002-2018  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZCOR_H
#define __ZCOR_H

#include <libziaint.h>

#include <glib.h>

#ifdef Z_HAVE_SDL 
#include <SDL.h>

#define COR_KM_MASK 0xffffff00
#define COR_KM_MIN  -19968
#define COR_KM_MAX  20224
#define COR_KM_STEP 256

struct cpoint{
  short int w,h;
  signed char c;
}
__attribute__((packed))
;

#ifdef Z_MSC
#pragma pack(push, 1)
#endif

struct kmpoint{
    short int kx,ky;
    signed char c;
}
#ifndef Z_ANDROID
__attribute__((packed))
#endif
;

#ifdef Z_MSC
#pragma pack(pop)
#endif

struct kmarray{
    struct kmpoint *data;
    int len;
    int size;
};

struct zcor{
	int nth;
    const struct cpoint *file;
    int items;
    gchar *loc;
    GHashTable *km; /* of struct ckmpoint */
    GHashTable *wwl4; /* of struct ckmpoint */
    GHashTable *wwl2; /* of struct ckmpoint */
	GHashTable *az; /* of struct ckmpoint */
    GThread *thread;
    int thread_break;
    double sw_myw, sw_myh;
	struct zselect *zsel;
	void (*callback)(void);

};

extern struct zcor *gcor;
extern double maxcnt;
//extern double gst[360];


struct kmarray *kmarray_new(void);
void kmarray_add(GHashTable *hash, gpointer key, struct kmpoint *km);

struct zcor *zcor_init(struct zselect *zsel, void(*callback)(void), const struct cpoint *file, int cor_items);
void zcor_free(struct zcor *cor);
void zcors_free(void);
struct zcor *zcor_calc(double myh, double myw, gchar *locator, struct zselect *zsel, void (*callback)(void), const struct cpoint *file, int cor_items);
void zcors_recalc(double myh, double myw, gchar *locator, struct zselect *zsel, void (*callback)(void), const struct cpoint *file, int cor_items);
gpointer zcor_thread_func(gpointer arg); 
void zcor_switch(void);
void zcor_read_handler(int n, char **items);
//gboolean free_km_item(gpointer key, gpointer value, gpointer user_data);
void free_km_value(gpointer value);
gpointer k2key(int kx, int ky);
void zcor_dump(struct zcor *cor);

#endif

#endif
