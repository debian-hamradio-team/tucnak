/*
    zandroid.c - Android JNI interface
    Copyright (C) 2012-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include <libziaint.h>

#ifdef Z_ANDROID

#include <zandroid.h>

#include <zdebug.h>
#include <zselect.h>

#include <jni.h>
#include <android/log.h>
#include <SDL.h>


static JavaVM *jvm;
static JNIEnv *genv;
static jobject gTucnakActivity;
static jclass gTucnakActivityClass;

static jmethodID updatePackage;
static jmethodID browser;
static jmethodID playWav;
static jmethodID ssbdAbort;
static jmethodID updateLocation;
static jmethodID messageBox;

struct zselect *zsel_location;

#ifdef Z_HAVE_SDL1
JNIEXPORT void JNICALL Java_cz_nagano_tucnak_MainActivity_nativeInitJavaCallbacks(JNIEnv *env, jobject thiz)
#endif
#ifdef Z_HAVE_SDL2
JNIEXPORT void JNICALL Java_cz_nagano_tucnak_TucnakActivity_nativeInitJavaCallbacks(JNIEnv *env, jobject thiz)
#endif
{
    __android_log_print(ANDROID_LOG_DEBUG, "tucnak", "nativeInitJavaCallbacks started env=%p thiz=%p -------------\n", env, thiz);
    __android_log_print(ANDROID_LOG_DEBUG, "tucnak", "nativeInitJavaCallbacks tid=%d\n", gettid());

    (*env)->GetJavaVM(env, &jvm);
    __android_log_print(ANDROID_LOG_DEBUG, "tucnak", "done\n");

    gTucnakActivity = (*env)->NewGlobalRef(env, thiz);
    gTucnakActivityClass = (*env)->GetObjectClass(env, thiz);
    //dbg("env=%p, gTucnakActivity=%p, gTucnakActivityClass=%p\n", env, gTucnakActivity, gTucnakActivityClass);
    updatePackage = (*env)->GetMethodID(env, gTucnakActivityClass, "UpdatePackage", "(Ljava/lang/String;)V");
    browser = (*env)->GetMethodID(env, gTucnakActivityClass, "Browser", "(Ljava/lang/String;)V");
    playWav = (*env)->GetMethodID(env, gTucnakActivityClass, "PlayWav", "(Ljava/lang/String;)V");
    ssbdAbort = (*env)->GetMethodID(env, gTucnakActivityClass, "SsbdAbort", "()V");
    updateLocation = (*env)->GetMethodID(env, gTucnakActivityClass, "UpdateLocation", "()V");
    messageBox = (*env)->GetMethodID(env, gTucnakActivityClass, "MessageBox", "(Ljava/lang/String;Ljava/lang/String;)V");

#ifdef Z_HAVE_SDL2
    SDL_SetHint(SDL_HINT_ANDROID_BLOCK_ON_PAUSE, "0");
#endif
    __android_log_print(ANDROID_LOG_DEBUG, "tucnak", "nativeInitJavaCallbacks finished\n");
}

// called after nativeInitJavaCallbacks
void zandroid_init(struct zselect *location){
    zsel_location = location;
    dbg("dbg zandroid_init tid=%d\n", gettid());
    
    int getEnvStat = (*jvm)->GetEnv(jvm, (void**)&genv, JNI_VERSION_1_6);
    if (getEnvStat == JNI_EDETACHED) {
        if ((*jvm)->AttachCurrentThread(jvm, (void**)&genv, NULL) != 0) {
            dbg("Cannot attach current thread to JVM");
        }
    }
}



void zandroid_update_package(const char *filename){
    dbg("zandroid_update_package('%s')\n", filename);
    //dbg("updatePackage=%p\n", updatePackage);
    jstring jFileName = (*genv)->NewStringUTF(genv, filename);
    //dbg("jFileName=%p\n", jFileName);
    (*genv)->CallVoidMethod(genv, gTucnakActivity, updatePackage, jFileName);
    dbg("zandroid_update_package return\n");
}

void zandroid_browser(const char *url){
    dbg("zandroid_browser('%s')\n", url);
    //dbg("updatePackage=%p\n", updatePackage);
    jstring jUrl = (*genv)->NewStringUTF(genv, url);
    //dbg("jFileName=%p\n", jFileName);
    (*genv)->CallVoidMethod(genv, gTucnakActivity, browser, jUrl);
    dbg("zandroid_browser return\n");
}
void zandroid_play_wav(const char *filename){
    dbg("zandroid_play_wav('%s')\n", filename);
    jstring jFileName = (*genv)->NewStringUTF(genv, filename);
    (*genv)->CallVoidMethod(genv, gTucnakActivity, playWav, jFileName);
    dbg("zandroid_play_wav return\n");
}

void zandroid_ssbd_abort(){
    dbg("zandroid_ssbd_abort()\n");
    (*genv)->CallVoidMethod(genv, gTucnakActivity, ssbdAbort);
    dbg("zandroid_ssbd_abort() RET\n");
}


static double myH, myW;
static int myState;

#ifdef Z_HAVE_SDL1
JNIEXPORT void JNICALL Java_cz_nagano_tucnak_MainActivity_nativeSendLocation(JNIEnv* env, jobject thiz, jdouble jh, jdouble jw, jint jstate) {
#endif
#ifdef Z_HAVE_SDL2
JNIEXPORT void JNICALL Java_cz_nagano_tucnak_TucnakActivity_nativeSendLocation(JNIEnv *env, jobject thiz, jdouble jh, jdouble jw, jint jstate){
#endif
    dbg("nativeSendLocation\n");
    myH = jh;
    myW = jw;
    myState = jstate;
	if (zsel_location) zselect_msg_send(zsel_location, "TERM");
}

void zandroid_get_location(double *h, double *w, int *state){
    //dbg("zandroid_get_location\n");
    *h = myH;
    *w = myW;
    *state = myState;
}

void zandroid_update_location(){
	dbg("zandroid_update_location()  tid=%d\n", gettid());
	myState = 0;
    (*genv)->CallVoidMethod(genv, gTucnakActivity, updateLocation);
}

void zandroid_messagebox(const char *caption, const char *str){
    dbg("zandroid_messagebox('%s', '%s')\n", caption, str);
    jstring jCap = (*genv)->NewStringUTF(genv, caption);
    jstring jStr = (*genv)->NewStringUTF(genv, str);
    (*genv)->CallVoidMethod(genv, gTucnakActivity, messageBox, jCap, jStr);
    dbg("zandroid_messagebox return\n");
}

#endif
