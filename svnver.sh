#!/bin/sh

svnver=`svnversion -n . 2>/dev/null`

if test "$?" != "0"; then
    exit 0
fi 

if test "$svnver" = "exported"; then # svn 1.6
    exit 0
fi

if test "$svnver" = "Unversioned directory"; then # svn 1.8
   exit 0
fi

echo "#define T_SVNVER \"$svnver\"" > src/svnversion.tmp
echo $svnver > svnver
cmp -s src/svnversion.h src/svnversion.tmp
if test $? = 0; then
    echo Keep svnversion $svnver
    exit 0
fi

echo Updating svnversion to $svnver
mv src/svnversion.tmp src/svnversion.h

exit 0

 
